# $@: The name of the target file (one before the colon)
# $<: The name of the first (or only) prerequisite file (first one after the colon)
# $^: The names of all the prerequisite files (space separated)
# $*: The stem (the bit which matches the % wildcard in the rule definition)

# Target: make / make debug / make clean / make remove

CXX = clang++
LINKER = clang++ -o
UNAME := $(shell uname)

CXXFLAGS = -O2 -Wall -march=native -mtune=native -pipe -std=c++11
ifeq ($(UNAME), Linux)
	LDFLAGS = -lm -lcrypt
endif
ifeq ($(UNAME), Darwin)
	LDFLAGS = -lm
endif

SRCDIR = src
OBJDIR = obj
BINDIR = bin

SOURCES := $(wildcard $(SRCDIR)/*.cc)
OBJECTS := $(SOURCES:$(SRCDIR)/%.cc=$(OBJDIR)/%.o)
TARGET  := $(SOURCES:$(SRCDIR)/%.cc=$(BINDIR)/%)
rm       = rm -f

debug: CXXFLAGS += -O0 -DDEBUG -g -ggdb
debug: $(TARGET)

$(TARGET): $(BINDIR)/% : $(OBJDIR)/%.o
	@mkdir -p $(BINDIR)
	@$(LINKER) $@ $(LDFLAGS) $<
	@echo "Linking $<... Done!"

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cc
	@mkdir -p $(OBJDIR)
	@$(CXX) $(CXXFLAGS) -c $< -o $@
	@echo "Compiling $<... Done!"

.PHONY: clean
clean:
	@$(rm) $(OBJECTS)
	@echo "Finish Cleanup!"

.PHONY: remove
remove: clean
	@$(rm) $(BINDIR)/$(TARGET)
	@echo "Removed executable!"
