#! /usr/bin/env python

# Generate source file and setup input directory
# Author: Han Zhao

import os, string, sys

# main driver
if __name__ == '__main__':

	pnum = sys.argv[1]
	source_dir = os.path.join(os.getcwd(), "src")
	source_name = os.path.join(source_dir, pnum) + ".cc"
	f = open(source_name, 'wb')
	# Beginning of file
	s = (   '/*\n'
			'  UVA online judge Problem : \n'
			'*/\n\n'
			'#include <algorithm>\n'
			'#include <bitset>\n'
			'#include <climits>\n'
			'#include <cmath>\n'
			'#include <deque>\n'
			'#include <iomanip>\n'
			'#include <iostream>\n'
			'#include <locale>\n'
			'#include <numeric>\n'
			'#include <queue>\n'
			'#include <set>\n'
			'#include <sstream>\n'
			'#include <stack>\n'
			'#include <string>\n'
			'#include <unordered_map>\n'
			'#include <unordered_set>\n'
			'#include <vector>\n\n'
			'using namespace std;\n\n'
			'int main()\n'
			'{\n'
			'  return 0;\n'
			'}\n\n'
		)
	f.write(s)
	f.close()

	#create input structure
	input_dir_name = "input/" + pnum
	input_dir = os.path.join(os.getcwd(), input_dir_name)
	os.mkdir(input_dir, 0755)
	input_file = input_dir + "/input1.txt"
	os.mknod(input_file)

