/*
  UVA online judge Problem P10258: Contest Scoreboard
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

// dim1 - contest ID 0-99
// dim2 - problem ID 0-8
using matrix_t = vector<vector<pair<bool,size_t>>>;

struct contest {
  size_t cid;
  size_t np;
  size_t penalty;
};

int main()
{
  size_t n;
  cin >> n;
  cin.ignore();

  for (size_t i = 0; i < n; ++i) {
    matrix_t ptime(vector<vector<pair<bool,size_t>>>(101, vector<pair<bool,size_t>>(10, make_pair(false, 0))));
    vector<contest> v(100);

    // if it's the first test case, ignore the first blank line
    if (i == 0) {
      cin.ignore();
    } else { // output blank line to separate from the last test case
      cout << endl;
    }
    string line;

    // process input
    while (getline(cin, line)) {
      if (line == "") break;
      istringstream ss(line);

      size_t cid, pid, t;
      char status;
      if (!(ss >> cid >> pid >> t >> status)) {
        cout << "Input file format error!" << endl;
        return 1;
      } else {
        if (status == 'R' || status == 'U' || status == 'E') {
          v[cid-1].cid = cid;
        } else if (status == 'I') {
          // if not solved yet
          if (ptime[cid][pid].first == false) {
            v[cid-1].cid = cid;
            ptime[cid][pid].second += 20;
          }
        } else if (status == 'C') {
          if (ptime[cid][pid].first == false) {
            ptime[cid][pid].first = true;
            ptime[cid][pid].second += t;
            v[cid-1].cid = cid;
            v[cid-1].np++;
          }
        } else {
          cout << "Incorrect status in input file!\n";
        }
      }
    }

    for (size_t i = 1; i <= 100; ++i) {
      for (size_t j = 1; j <= 9; ++j) {
        if (ptime[i][j].first == true) {
          v[i-1].penalty += ptime[i][j].second;
        }
      }
    }

    // sort
    sort(v.begin(), v.end(), [](const contest& c1, const contest& c2){
      if (c1.np > c2.np) {
        return true;
      } else if (c1.np == c2.np) {
        if (c1.penalty < c2.penalty) {
          return true;
        } else if (c1.penalty == c2.penalty) {
          return c1.cid < c2.cid ? true : false;
        } else {
          return false;
        }
      } else {
        return false;
      }
    });

    // output
    for (size_t i = 0; i < v.size(); ++i) {
      if (v[i].cid > 0) {
        cout << v[i].cid << " " << v[i].np << " " << v[i].penalty << endl;
      }
    }

  }

  return 0;
}

