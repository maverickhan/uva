/*
  UVA online judge Problem P10152: ShellSort.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();

  for (size_t i = 0; i < ntest; ++i) {
    size_t n;
    cin >> n;
    cin.ignore();

    vector<string> turtle_stack(n);
    unordered_map<string, int> rank;

    for (size_t i = 0; i < n; ++i) {
      getline(cin, turtle_stack[i]);
    }

    string name;
    for (int i = 0; i < n; ++i) {
      getline(cin, name);
      rank.insert(pair<string, int>(name, i));
    }

    // first figure out out-of-order elements
    vector<string> out_of_order;
    vector<string> in_order;
    in_order.push_back(turtle_stack[0]);
    int max = rank[turtle_stack[0]];
    int list_max = INT_MIN;
    for (size_t i = 1; i < n; ++i) {
      int curr_val = rank[turtle_stack[i]];

      if (curr_val < max) {
        if (curr_val > list_max) {
          list_max = curr_val;
        }
        out_of_order.push_back(turtle_stack[i]);
      } else {
        max = curr_val;
        in_order.push_back(turtle_stack[i]);
      }
    }

    // since we have a new out-of-order set, some of the remaining
    // elements might become out of order due to continuous update
    // of the max element in the ood set. Therefore, we need a second
    // pass to find the out-of-order elements in the rest of the elements.
    // However, we are not going to need a third pass because only elements
    // less than the ood max is added to ood so ood max is not updated.
    for (const auto& i : in_order) {
      if (rank[i] < list_max) {
        out_of_order.push_back(i);
      }
    }

    // now we only need to sort the rest elements in decending order
    // such that the operations yiled a minimum-path sorted-order.
    sort(out_of_order.begin(), out_of_order.end(),
         [&rank](const string& a, const string& b){return rank[a] > rank[b];});

    for (const auto& i : out_of_order) {
      cout << i << endl;
    }

    cout << endl;
  }

  return 0;
}
