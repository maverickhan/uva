/*
  UVA online judge Problem P10026: Shoemaker's Problem.
  Job i's incurred cost to job j is calculated as: Ti * Sj
  Therefore, Ti is before Tj only when Ti * Sj < Tj * Si
  We also use id to break tie for lexicographically ordered output.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

struct job {
  int id;
  int time;
  int cost;

  job(int i, int t, int s) : id(i), time(t), cost(s) {}
};

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();

  string line;
  for (size_t i = 0; i < ntest; ++i) {
    size_t n;
    cin >> n;
    cin.ignore();
    vector<job> v;
    for (size_t j = 0; j < n; ++j) {
      int t, s;
      cin >> t >> s;
      cin.ignore();
      v.push_back(job(j+1, t, s));
    }

    sort(v.begin(), v.end(), [](const job& a, const job& b){
      if(a.time * b.cost < b.time * a.cost) {
        return true;
      } else if (a.time * b.cost == b.time * a.cost) {
        return a.id < b.id;
      }

      return false;
    });

    for (size_t k = 0; k < n; ++k) {
      cout << v[k].id;
      if (k != n-1) {
        cout << " ";
      }
    }
    cout << endl;

    // separate two test cases
    if (i != ntest-1) {
      cout << endl;
    }
  }

  return 0;
}
