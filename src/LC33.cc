/*
  Leet Code 33: Search in Rotated Sorted Array
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int searchRotatedArray(const vector<int>& v, int target)
{
  int n = int(v.size());

  int low = 0;
  int high = n;

  while(low <= high) {
    int mid = low + (high - low) / 2;
    if (v[mid] == target) return mid;
    else if (v[mid] < target) {
      if (v[mid] > v[0] && v[mid] > v[n-1]) { // larger section
        low = mid+1;
      } else {
        if (v[n-1] == target) return n-1;
        else if (v[n-1] > target) low = mid+1;
        else high = mid-1;
      }
    } else if (v[mid] > target){
      if (v[mid] < v[0] && v[mid] < v[n-1]) { // smaller section
        high = mid-1;
      } else {
        if (v[0] == target) return 0;
        else if (v[0] < target) high = mid-1;
        else low = mid+1;
      }
    }
  }

  return -1;
}

int main()
{
  string line;
  getline(cin, line);

  stringstream ss(line);
  string num;
  vector<int> v;
  while(getline(ss, num, ' ')) {
    v.push_back(stoi(num));
  }

  size_t k;
  cin >> k;
  cin.ignore();

  int target;
  for (size_t i = 0; i < k; ++i) {
    cin >> target;
    cin.ignore();

    int result = searchRotatedArray(v, target);

    cout << target << ": " << result << endl;
  }

  return 0;
}

