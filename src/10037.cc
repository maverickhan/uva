/*
  UVA online judge Problem P10037: Bridge
  First sort based on speed.
  On each round, there are four cases:
   - 1 person, simple
   - 2 people, simple
   - 3 people, only one strategy to move the slowest two
   - >=4 people, two strategies to move the slowest two
   Go round by round until no one is left on the left side of the river.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();

  for (size_t i = 0; i < ntest; ++i) {
    size_t n;
    cin >> n;
    cin.ignore();

    vector<int> v(n);

    // read in initial speed for n people
    for (size_t j = 0; j < n; ++j) {
      int x;
      cin >> x;
      v[j] = x;
      cin.ignore();
    }

    int cross_time = 0;
    // suppose v = {a, b, ..., y, z}
    sort(v.begin(), v.end());

    vector<string> cross_history;
    int remain = n;
    while (remain > 0) {
      // a
      if (remain == 1) {
        cross_time += v[0];
        cross_history.push_back(to_string(v[0]));
        remain -= 1;
      } else if (remain == 2) { // a, b
        cross_time += v[1];
        cross_history.push_back(to_string(v[0]) + " " + to_string(v[1]));
        remain -= 2;
      } else if (remain == 3) { // a, b, z
        // ab-> a<- az->
        cross_time += (v[0] + v[1] + v[2]);
        cross_history.push_back(to_string(v[0]) + " " + to_string(v[1]));
        cross_history.push_back(to_string(v[0]));
        cross_history.push_back(to_string(v[0]) + " " + to_string(v[2]));
        remain -= 3;
      } else if (remain >= 4) {
        int t1 = v[0] + 2 * v[1] + v[remain-1]; // ab-> a<- yz-> b<-
        int t2 = 2 * v[0] + v[remain-2] + v[remain-1]; // ay-> a<- az-> a<-
        if (t1 < t2) {
          cross_time += t1;
          cross_history.push_back(to_string(v[0]) + " " + to_string(v[1]));
          cross_history.push_back(to_string(v[0]));
          cross_history.push_back(to_string(v[remain-2]) + " " + to_string(v[remain-1]));
          cross_history.push_back(to_string(v[1]));
        } else {
          cross_time += t2;
          cross_history.push_back(to_string(v[0]) + " " + to_string(v[remain-2]));
          cross_history.push_back(to_string(v[0]));
          cross_history.push_back(to_string(v[0]) + " " + to_string(v[remain-1]));
          cross_history.push_back(to_string(v[0]));
        }
        remain -= 2;
      }
    }

    cout << cross_time << endl;
    for (const auto& i : cross_history) {
      cout << i << endl;
    }

    // separate two cases
    if (i != ntest-1) {
      cout << endl;
    }
  }

  return 0;
}
