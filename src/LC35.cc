/*
  Leet Code 35: Search Insert Position
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

size_t findInsertPos(vector<int>& v, int t, size_t low, size_t high)
{
  if (low > high) {
    return low-1;
  }

  if (low == high) {
    if (t == v[high]) return high;
    else if (t > v[high]) return high+1;
    else {
      if (high == 0) return 0;
      else return high-1;
    }
  }

  int mid = v[low+(high-low)/2];
  if (v[mid] == t) return mid;
  else if (v[mid] < t) return findInsertPos(v, t, low+1, high);
  else return findInsertPos(v, t, low, high-1);
}

int main()
{
  size_t k;
  cin >> k;
  for (size_t i = 0; i < k; ++i) {
    cin.ignore();
    string line;
    getline(cin, line);
    stringstream ss(line);
    string num;
    vector<int> v;
    while(getline(ss, num, ' ')) {
      v.push_back(stoi(num));
    }

    int target;
    cin >> target;
    size_t pos = findInsertPos(v, target, 0, v.size()-1);

    cout << pos << endl;
  }

  return 0;
}

