/*
  UVA online judge Problem P10150: Doublets
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

struct node {
  string word;
  bool   check;
  bool   visited;
  node*  parent;

  node(const string& s) : word(s) {
    check = false;
    visited = false;
    parent = nullptr;
  }

  node& operator=(const node& rhs) {
    // avoid self-assignment
    if (this == &rhs) {
      return *this;
    }

    this->word = rhs.word;
    this->check = rhs.check;
    this->visited = rhs.visited;
    this->parent = rhs.parent;

    return *this;
  }
};

using node_t  = node;
using queue_t = deque<node_t*>;
using dict_t  = unordered_map<size_t, vector<node_t>>;

bool is_doublet(const string& s1, const string& s2)
{
  size_t sz = s1.size();
  if (sz != s2.size()) return false;

  size_t num_diff = 0;
  for (size_t i = 0; i < sz; ++i) {
    if (s1[i] != s2[i]) {
      num_diff++;
      if (num_diff > 1) return false;
    }
  }

  if (num_diff == 1)
    return true;

  return false;
}

int main()
{
  dict_t D;
  string line;

  // read in dictionary
  do {
    getline(cin, line);
    size_t sz = line.size();
    if (D.find(sz) != D.end()) {
      D[sz].push_back(node_t(line));
    } else {
      D.insert(pair<size_t, vector<node_t>>(sz, vector<node_t>(1, node_t(line))));
    }
  } while(!line.empty());

  vector<string> texts;
  while(getline(cin, line)) {
    texts.push_back(line);
  }

  string src_str, dest_str;
  for (size_t i = 0; i < texts.size(); ++i) {
    stringstream ss(texts[i]);
    ss >> src_str >> dest_str;

    if (src_str.size() != dest_str.size()) {
      cout << "No solution." << endl;
    } else {
      // Create graph
      // First clear dictionary state
      queue_t q;
      size_t sz = src_str.size();

      // clear the ground and search for src node in dictionary
      for (auto& v: D[sz]) {
        if (v.word == src_str) {
          q.push_back(&v);
        }
        v.check = false;
        v.visited = false;
        v.parent = nullptr;
      }

      // breadth-first search
      node_t* curr = q.front();
      curr->check = true;

      bool found_dest = false;
      vector<string> path;
      while(!q.empty() && !found_dest) {
        for (auto& v: D[sz]) {
          if (!v.check && !v.visited && is_doublet(curr->word, v.word)) {

            // found destination string
            if (v.word == dest_str) {
              v.parent = curr;
              found_dest = true;
              // backtrack to record path
              path.push_back(v.word);
              node_t* iv = &v;
              while (iv->parent != nullptr) {
                iv = iv->parent;
                path.push_back(iv->word);
              }
              break;
            } else {
              v.parent = curr;
              v.check = true;
              q.push_back(&v);
            }
          }
        }

        if (!found_dest) {
          curr->visited = true;
          q.pop_front();
        }

        curr = q.front();
      }

      if (q.empty()) {
        cout << "No solution." << endl;
      }

      for (auto it = path.rbegin(); it != path.rend(); ++it) {
        cout << *it << endl;
      }
    }

    if (i != texts.size()-1) {
      cout << endl;
    }
  }

  return 0;
}
