/*
  UVA online judge Problem :
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

void genGrayCode(vector<int>& gc, int n)
{
  if (n == 1) {
    gc.push_back(0);
    gc.push_back(1);

    return ;
  }

  genGrayCode(gc, n-1);
  size_t len = gc.size();
  for (size_t i = 0; i < len; ++i) {
    gc.push_back(int(pow(2, n-1)) + gc[len-1-i]);
  }
}

int main()
{
  int n;

  while (cin >> n) {
    vector<int> gcode;
    genGrayCode(gcode, n);

    for (const auto& c : gcode) {
      cout << c << " ";
    }
    cout << endl;
  }

  return 0;
}

