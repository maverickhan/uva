/*
  UVA online judge Problem P10188: Automated Judge Script
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

string extract_numerical(const string& s)
{
	string ns;
	for (const auto& c : s) {
		if (isdigit(c)) {
			ns.append(1, c);
		}
	}

	return ns;
}

int main()
{
	size_t ntest = 1;
	
	int nlines;
	string line;
	cin >> nlines;
	while (nlines != 0) {
		cin.ignore();
		string sys_out = "";
		for (int i = 0; i < nlines; ++i) {
			getline(cin, line);
			sys_out.append(line);
			sys_out.append(1, '\n');
		}

		int user_nlines;
		cin >> user_nlines;
		cin.ignore();

		string user_out = "";
		for (int i = 0; i < user_nlines; ++i) {
			getline(cin, line);
			user_out.append(line);
			user_out.append(1, '\n');
		}

		bool correct = false;
		bool pe = false;

		if (sys_out.compare(user_out) == 0) {
			correct = true;
		} else {
			string ns1 = extract_numerical(sys_out);
			string ns2 = extract_numerical(user_out);
			if (ns1.compare(ns2) == 0) {
				pe = true;
			}
		}

		if (correct) {
			cout << "Run #" << ntest << ": Accepted" << endl;
		} else if (pe) {
			cout << "Run #" << ntest << ": Presentation Error" << endl;
		} else {
			cout << "Run #" << ntest << ": Wrong Answer" << endl;
		}

		ntest++;
		cin >> nlines;
	}

  return 0;
}
