/*
  UVA online judge Problem : Queue
  There is a queue with N people. Every person has a different heigth. We can see P people, when we
  are looking from the beginning, and R people, when we are looking from the end. Its because they
  are having different height and they are covering each other. How many different permutations of our
  queue has such a interesting feature?
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  // Calculate Pascal's Triangle
  vector<vector<int>> c(14, vector<int>(14));
  c[0][0] = 1;
  for (int i = 1; i < 14; ++i) {
  	c[i][0] = c[i][i] = 1;
  }
  for (int i = 2; i < 14; ++i) {
  	for (int j = 1; j < i; ++j) {
  		c[i][j] = c[i-1][j] + c[i-1][j-1];
  	}
  }

  vector<vector<vector<int>>> v(14, vector<vector<int>>(14, vector<int>(14)));
  v[1][1][1] = v[0][0][0] = 1;
  for (int n = 2; n < 14; ++n) {
  	for (int p = 1; p <= n; ++p) {
  		for (int r = 1; r <= p; ++r) {
  			for (int i = p-1; i <= n-1-(r-1); ++i) {
  				v[n][p][r] += c[n-1][i] * accumulate(v[i][p-1].begin(), v[i][p-1].end(), 0)
  									    * accumulate(v[n-1-i][r-1].begin(), v[n-1-i][r-1].end(), 0);
  				v[n][r][p] = v[n][p][r];
  			}
  		}
  	}
  }

  size_t num_test;
  cin >> num_test;

  int n, p, r;
  for (size_t k = 0; k < num_test; ++k){
  	cin >> n >> p >> r;
  	if (p == 0 || r == 0 || p+r > n+1) {
  		cout << "0" << endl;
  		continue;
  	}
  	cout << v[n][p][r] << endl;
  }

  return 0;
}

