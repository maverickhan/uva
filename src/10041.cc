/*
  UVA online judge Problem P10041: Vito's Family
  Median number minimizes the distance to all in the set
  If there are even number of elements, median is defined as the average
  of the two middle numbers.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();

  string line;
  for (size_t i = 0; i < ntest; ++i) {
    getline(cin, line);
    stringstream ss(line);
    string num;
    getline(ss, num, ' ');
    int r = stoi(num);
    vector<int> v(r);
    int sum = 0;
    for (int i = 0; i < r; ++i) {
      getline(ss, num, ' ');
      v[i] = stoi(num);
      sum += v[i];
    }

    sort(v.begin(), v.end());
    int median = v[r / 2];
    int distance = 0;
    for (size_t i = 0; i < r / 2; ++i) {
      distance += median - v[i];
    }
    for (size_t i = r / 2 + 1; i < r; ++i) {
      distance += v[i] - median;
    }

    cout << distance << endl;
  }

  return 0;
}
