/*
  UVA online judge Problem P10252: Common Permutation
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  // processing inputs
  vector<string> v;
  string line;
  while (getline(cin, line)) {
    v.push_back(line);
  }

  for (size_t i = 0; i < v.size(); i+=2) {
    // sort the two strings
    sort(v[i].begin(), v[i].end());
    sort(v[i+1].begin(), v[i+1].end());
    unordered_map<char, size_t> a;
    unordered_map<char, size_t> b;

    for (size_t i = 0; i < 26; ++i) {
      char key = static_cast<char>('a' + i);
      a.insert(pair<char, size_t>(key, 0));
      b.insert(pair<char, size_t>(key, 0));
    }

    for (const auto& c : v[i]) {
      a[c]++;
    }

    for (const auto& c : v[i+1]) {
      b[c]++;
    }

    string x;
    for (size_t i = 0; i < 26; ++i) {
      char key = static_cast<char>('a' + i);
      size_t n = min(a[key], b[key]);
      x.append(n, key);
    }
    cout << x << endl;
  }

  return 0;
}
