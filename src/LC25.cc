/*
  LeetCode 25: Reverse Nodes in k-group 
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

struct node {
  int _val;
  node* _next;

  node() : _val(0), _next(nullptr) {}
  node(int x) : _val(x), _next(nullptr) {}
};

void printList(node* head)
{
  if (head == nullptr) return ;
  node* curr = head;
  do {
    cout << curr->_val << "->";
    curr = curr->_next;
  } while (curr != nullptr);
  cout << endl;
}

node* swapKGroup(node* head, int k)
{
  if (head == nullptr) return nullptr;
  if (k < 2) return head;

  node* new_head = nullptr;

  stack<node*> st;
  int counter = 0;
  bool firstgroup = true;
  node* curr = head;
  node* tail = nullptr;

  while(curr != nullptr) {
    st.push(curr);
    counter++;
    curr = curr->_next;

    if (counter == k) {
      // first group, update head
      if (firstgroup) {
        new_head = st.top();
      }
      // last tail pointing to the new group's head
      if (tail != nullptr) {
        tail->_next = st.top();
      }
      while(st.size() > 1) {
        node* topnode = st.top();
        st.pop();
        topnode->_next = st.top();
      }

      // to handle the last node
      tail = st.top();
      tail->_next = curr;
      st.pop();
      counter = 0;

      firstgroup = false;
    }
  }

  // now we handle the last tail which should point to the un-altered rest
  // note that the stack might not have any node left
  while (st.size() > 1) {
    st.pop();
  }
  if (tail != nullptr && !st.empty()) {
    tail->_next = st.top();
  }

  return new_head;
}

int main()
{
  string line;
  getline(cin, line);
  stringstream ss(line);
  node* head = nullptr;
  string value;
  getline(ss, value, ' ');
  if (!value.empty()) {
    head = new node(stoi(value));
    node* curr = head;
    while (getline(ss, value, ' ')) {
      node* next = new node(stoi(value));
      curr->_next = next;
      curr = next;
    }
  }

  printList(head);

  int k;
  cin >> k;
  cin.ignore();
  cout << "Reverse list in " << k << "-group:" << endl;

  node* new_head = swapKGroup(head, k);
  printList(new_head);

  return 0;
}

