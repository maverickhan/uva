/*
  LeetCode 146: LRU Cache
  Policy - Least recently used
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <list>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

class cache_policy;

struct cache {
  int _capacity;
  int _size;
  // key-value store of cache elements.
  unordered_map<int, int> _c;

  // data structure for holding access history
  // most recently accessed key is inserted to front
  // including newly insert/update/read
  // Evict from back of the double-linked list.
  list<int> _acchistory;

  cache(){}
  cache(int cap, int sz = 0) : _capacity(cap), _size(sz) {}
  cache(const cache& rhs) {
    _c = rhs._c;
    _acchistory = rhs._acchistory;
  }
  cache& operator=(const cache& rhs) {
    if (this != &rhs) {
      _c.clear();
      _acchistory.clear();

      _c = rhs._c;
      _acchistory = rhs._acchistory;
    }
    return *this;
  }

  template <typename Policy>
  void put(const int key, const int val)
  {
    if (_c.find(key) != _c.end()) {
      cout << "Update " << key << ": " << val << endl;
      auto it = _acchistory.begin();
      while (it != _acchistory.end()) {
        if (*it == key) break;
        it++;
      }
      _acchistory.erase(it);
      _acchistory.push_front(key);
      _c[key] = val;
      return ;
    } else {
      if (_size == _capacity) {
        Policy::lru_evict(*this);
      }

      _c.insert(make_pair(key, val));
      _acchistory.push_front(key);
      cout << "Insert " << key << ": " << val << endl;
      _size++;
    }
  }

  int get(const int key)
  {
    if (_c.find(key) != _c.end()) {
      cout << "Hit: " << key << endl;
      // delete and re-insert to back
      int val = _c[key];

      // Note that we could potentially avoid search linearly everytime
      // by saving the pointer to element in the hashmap.
      auto it = _acchistory.begin();
      while(it != _acchistory.end()) {
        if (*it == key) break;
        it++;
      }
      _acchistory.erase(it);
      _acchistory.push_front(key);
      return val;
    }

    cout << "Miss: " << key << endl;
    return -1;
  }

};

// policy class for cache eviction
class cache_policy {
public:
  static void lru_evict(cache& c) {
    auto key = c._acchistory.back();
    cout << "Evict " << key << endl;
    c._c.erase(key);
    c._acchistory.pop_back();
    c._size--;
  }
};

int main()
{
  size_t capacity;
  cin >> capacity;
  cin.ignore();

  cache c(capacity);
  cout << "Create cache with capacity: " << c._capacity << endl;

  string line;
  while (getline(cin, line)) {
    stringstream ss(line);
    string token;
    getline(ss, token, ' ');
    if (token == "PUT") {
      getline(ss, token, ' ');
      int key = stoi(token);
      getline(ss, token, ' ');
      int value = stoi(token);
      c.put<cache_policy>(key, value);
    } else if (token == "GET") {
      getline(ss, token, ' ');
      int key = stoi(token);
      c.get(key);
    }
  }

  return 0;
}

