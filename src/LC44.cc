/*
  Leetcode 44: Wildcard Matching
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

bool isMatchHelper(const string& s, const string& p, size_t s_begin, size_t p_begin)
{
  size_t m = s.size();
  size_t n = p.size();

  if (s_begin == m && p_begin == n) {
    return true;
  }

  if (s_begin == m || p_begin == n) {
    return false;
  }

  if (p[p_begin] != '*') {
    if (p[p_begin] == '?' || s[s_begin] == p[p_begin]) {
      return isMatchHelper(s, p, s_begin+1, p_begin+1);
    }
    else {
      return false;
    }
  } else {
    bool tryMatch = false;
    for (size_t i = s_begin; i <= m; ++i) {
       tryMatch = isMatchHelper(s, p, i, p_begin+1);
       if (tryMatch) return true;
    }
    return false;
  }

  return false;
}

bool isMatch(const string& s, const string& p)
{
  if (s.empty() || p.empty()) {
    return false;
  }

  return isMatchHelper(s, p, 0, 0);
}

int main()
{
  string line;
  while (getline(cin, line)) {
    stringstream ss(line);
    string s, p;
    getline(ss, s, ' ');
    getline(ss, p, ' ');

    bool rlt = isMatch(s, p);
    string output = rlt ? "true" : "false";

    cout << output << endl;
  }

  return 0;
}

