/*
  Leet Code 22: Swap nodes in pairs
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

struct node {
  int _val;
  node* _next;

  node() : _val(0), _next(nullptr) {}
  node(int x) : _val(x), _next(nullptr) {}
};

void printList(node* head)
{
  if (head == nullptr) return ;
  node* curr = head;
  do {
    cout << curr->_val << "->";
    curr = curr->_next;
  } while (curr != nullptr);
  cout << endl;
}

void swapPairHelper(node* prev, node* curr)
{
  if (curr == nullptr) return ;

  node* new_prev;
  node* new_curr;
  new_prev = curr->_next;
  if (new_prev != nullptr) {
    new_curr = new_prev->_next;
  } else {
    curr->_next = prev;
    prev->_next = nullptr;
    return ;
  }

  curr->_next = prev;
  prev->_next = new_curr == nullptr ? new_prev : new_curr;
  swapPairHelper(new_prev, new_curr);
}

node* swapPair(node* head)
{
  if (head == nullptr) return nullptr;

  node* prev = head;
  node* curr = head->_next;
  if (curr == nullptr) return head;
  swapPairHelper(prev, curr);
  return curr;
}

int main()
{
  string line;
  while (getline(cin, line)) {
    stringstream ss(line);
    node* head = nullptr;
    string value;
    getline(ss, value, ' ');
    if (!value.empty()) {
      head = new node(stoi(value));
      node* curr = head;
      while (getline(ss, value, ' ')) {
        node* next = new node(stoi(value));
        curr->_next = next;
        curr = next;
      }
    }

    printList(head);

    node* new_head = swapPair(head);
    printList(new_head);
  }

  return 0;
}

