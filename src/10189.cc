/*
  UVA online judge Problem #10189: Minesweeper
  - cin >> var, needs to call cin.ignore() to flush
    the newline character.
  - vec.reserve(n), only allocate memory but do not
    add new elements, need to call push_back later.
  - vec.resize(n) adds capacity and initialize, later
    can use [] or at to modify elements.
  - Update of matrix entries affect other entries
  - Clear vector when proceeding to the next set.
 */

#include <algorithm>
#include <climits>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

using idx = pair<size_t, size_t>;
vector<idx> mines;

void
read_vector(vector<int>& v, size_t n, size_t i)
{
  string line, num;
  getline(cin, line);
  for (size_t j = 0; j < n; j++) {
    if (line[j] == '*') {
      v.push_back(-1);
      mines.push_back(idx(i, j));
    } else {
      v.push_back(0);
    }
  }
}

int main()
{
  vector<vector<int>> board;

  size_t m, n;
  cin >> m >> n;
  // cin.ignore is required to flush the newline char
  cin.ignore();

  if (m == 0 && n == 0) {
    return 0;
  }

  size_t cnt = 1;
  while (m > 0 && n > 0) {
    if (cnt > 1) {
      cout << endl;
    }

    cout << "Field #" << cnt << ":" << endl;
    board.resize(m);
    for (size_t i = 0; i < m; ++i) {
      board[i].reserve(n);
      read_vector(board[i], n, i);
    }

    for (const auto i : mines) {
      int x = i.first;
      int y = i.second;

      for (int u = x-1; u <= x+1; ++u) {
        for (int v = y-1; v <= y+1; ++v) {
          if (u >= 0 && u < m &&
              v >= 0 && v < n &&
              board[u][v] >= 0) {
            board[u][v]++;
          }
        }
      }
    }

    for (size_t i = 0; i < m; ++i) {
      for (size_t j = 0; j < n; ++j) {
        if (board[i][j] < 0) {
          cout << '*';
        }
        else {
          cout << board[i][j];
        }
      }
      cout << endl;
    }

    cin >> m >> n;
    cin.ignore();
    board.clear();
    mines.clear();
    cnt++;
  }

  return 0;
}