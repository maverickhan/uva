/*
  UVA online judge Problem p10137: The Trip
   *** WA ***
  - Note how to compare double with 0
    and how to test equality with float and double
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

double rounded(const double n)
{
  double temp = n * 100;
  double intpart;
  double frac = modf(temp, &intpart);
  temp = frac >= 0.5 ? ceil(temp) : floor(temp);
  return temp / 100;
}

int main()
{
  size_t n;
  vector<double> cost;

  cin >> n;
  cin.ignore();
  while (n > 0) {
    string line;
    double sum = 0.0;
    double ea = 0.0;
    double eb = 0.0;
    for (size_t i = 0; i < n; ++i) {
      getline(cin, line);
      double x = stof(line);
      sum += x;
      cost.push_back(x);
    }

    double avg = rounded(sum / n);
    for (size_t i = 0; i < n; ++i) {
      double x = cost[i] - avg;
      if (x > 0) {
        ea += x;
      } else if (x < 0) {
        eb -= x;
      }
    }

    double exchange;
    double epsilon = 1e-5;
    if (abs(ea) < epsilon && abs(eb) < epsilon) {
      exchange = 0.0;
    } else if (abs(ea) < epsilon && abs(eb) >= epsilon) {
      exchange = eb - 0.01;
    } else if (abs(eb) < epsilon && abs(ea) >= epsilon) {
      exchange = ea - 0.01;
    } else {
      exchange = ea < eb ? ea : eb;
    }

    cout << fixed;
    cout << setprecision(2) << exchange << endl;

    cost.clear();
    cin >> n;
    cin.ignore();
  }

  return 0;
}
