/*
  LeetCode 152: Maximum Product Subarray
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int maxProductSubarray(const vector<int>& v)
{
  size_t n = v.size();
  int maxp = v[0];
  int curr_max = v[0], curr_min = v[0];

  // Note that previous min can become current max due to the product
  // of two negative numbers
  for (size_t i = 1; i < n; ++i) {
    int temp = curr_max;
    curr_max = max(max(curr_max * v[i], curr_min * v[i]), v[i]);
    curr_min = min(min(temp * v[i], curr_min * v[i]), v[i]);
    maxp = max(maxp, curr_max);
  }

  return maxp;
}

int main()
{
  size_t num_test;
  cin >> num_test;
  cin.ignore();

  for (size_t k = 0; k < num_test; ++k) {
    string line;
    getline(cin, line);
    stringstream ss(line);
    string num;
    vector<int> v;
    while (getline(ss, num, ' ')) {
      v.push_back(stoi(num));
    }

    int max_product = maxProductSubarray(v);
    cout << max_product << endl;
  }

  return 0;
}

