/*
  UVA online judge Problem P10157: Expressions
  Let f(l,d) be the function to calculate the valid number of parenthesis
  with l pairs and depth of at most d

  consider the leftmost parenthiss dividing the expression
  to two parts: inside and outside

  inside's number of pairs could range from 0 to l-1, and has depth d-1
  outside has l-k-1(1 represents the outermost one), and has depth d

  f(l,d) = sum_{i=1...l-1}f(i, d-1)f(k-i-1, d)
  f(l,1) = 1 (l > 0)
  Then for l pairs with depth d, the result is f(l,d) - f(l,d-1)

  DIMote also this problem needs library size_t to represent number of solutions
  outside the range of size_t
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "header/infint.hh"

// maximum number of parenthesis pair and depth
#define DIM 151

using namespace std;

using bigint = InfInt;

void preprocess_f(vector<vector<bigint>>& F)
{
  for (size_t l = 1; l < DIM; ++l) {
    for (size_t d = 1; d < DIM; ++d) {
      for (size_t k = 0; k < l; ++k) {
        F[l][d] += F[k][d-1] * F[l-k-1][d];
      }
    }
  }
}

int main()
{
  // matrix to store fn(k, d), where k is the number of parenthesis pairs,
  // and d is the depth, fn's output is the number of valid parenthesis.
  // k <= 150 and d <= 150
  vector<vector<bigint>> F(DIM, vector<bigint>(DIM, 0));

  // base case
  // result[0][i] = F[0][i] - F[0][i-1] = 0
  for (size_t i = 0; i < DIM; ++i) {
    F[0][i] = 1;
  }

  preprocess_f(F);

  string line;
  while (getline(cin, line)) {
    if (line.empty()) {
      continue;
    }
    istringstream iss(line);
    int length;
    int depth;

    iss >> length >> depth;
    if (length % 2 != 0 || depth == 0) {
      cout << "0" << endl;
    } else {
      int l = length / 2;
      bigint result = F[l][depth] - F[l][depth-1];
      cout << result << endl;
    }
  }

  return 0;
}

