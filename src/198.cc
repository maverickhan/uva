/*
  UVA online judge Problem #198: Peter's Calculator
  https://uva.onlinejudge.org/external/1/198.pdf
 */

#include "header/common.hh"

#define UNDEF INT_MIN

using namespace std;
using namespace uva;

using table_t = unordered_map<string, string>;
using opstack = stack<char>;
using valstack = stack<int>;

void remove_space(string& input)
{
  input.erase(remove(input.begin(), input.end(), ' '), input.end());
}

int read_var(const string& s, const int i)
{
  int j = i+1;
  while (j < s.size() && (s[j] != '+' &&
                          s[j] != '-' &&
                          s[j] != '*' &&
                          s[j] != '(' &&
                          s[j] != ')')) {
    j++;
  }

  return j-i;
}

// Expand var to expression where expression only consists of numbers
// and operators.
// In case of finding a new variable, substitute with its expression
// If variable is recursively defined, return UNDEF
// For each newly expanded variable, add '()' to ensure correctness.
int expand(const string& var, string& s, table_t& tbl)
{
  table_t::const_iterator iter = tbl.find(var);
  if (iter == tbl.end() || tbl[var] == var) {
    return UNDEF;
  } else {
    s = tbl[var];
  }

  // initialize the list of vars encountered along the way
  // if any newly found var falls in this list then we have
  // a cycle.
  std::vector<string> var_string;
  var_string.push_back(var);
  int len;
  for (int i = 0; i < s.size(); ++i) {
    // Find start of a var
    if (isalpha(s[i])) {
      int j = i+1;
      while(isalpha(s[j]) || isdigit(s[j]))
        j++;
      len = j-i;
      string new_var = s.substr(i, len);
      auto it = tbl.find(new_var);
      if (it == tbl.end()) {
        return UNDEF;
      } else {

        // find cycle definition
        for (auto it = var_string.begin(); it != var_string.end(); ++it) {
          // cycle
          if (new_var == *it) {
            return UNDEF;
          }
        }
        var_string.push_back(new_var);

        // replace old variable [i, i+len] with (tbl[new_var])
        string new_s = "()";
        new_s.insert(1, tbl[new_var]);
        s.replace(i, len, new_s);
      }
    }
  }

  // Successful expand
  return 0;
}

// when op is '+', '-' or '*', peek two topmost values
// in val stack. If either is undefined or expression,
// simply return UNDEF or EXPR. In the case of EXPR,
// a new expression is pushed onto both ops and vals stack
// according to the table. In case of both values are
// normal integers, return the computational result.
void pop_op(opstack& ops, valstack& vals, table_t& tbl)
{
  if (ops.empty()) {
    return ;
  }

  int a = vals.top();
  vals.pop();
  int b = vals.top();
  vals.pop();

  char op = ops.top();
  ops.pop();

  int val;
  if (op == '+') {
    val = a + b;
  } else if (op == '-') {
    val = b - a;
  } else if (op == '*') {
    val = a * b;
  }

  vals.push(val);
}

// Evaluate 'expr' where expr consists of numbers and opeartors
int evaluate(const string& s, table_t& t)
{
  opstack ops;
  valstack vals;

  string curr_var;

  int i = 0;
  while (i < s.size()) {

    // if it's '(', simply push to op stack
    if (s[i] == '(' ) {
      ops.push(s[i]);
      i++;
    }

    // if it's ')', pop until see matching '('
    else if (s[i] == ')') {
      if (!ops.empty()) {
        while (ops.top() != '(') {
          pop_op(ops, vals, t);
        }
        // pop out matching '('
        ops.pop();
      }
      i++;
    }

    // if it's '-', we need to figure out if this is the real op
    else if (s[i] == '-') {
      // ignore all '()' in between and check the previous sign
      // if hit head, then '-' is the negative sign
      int j = i-1;
      while (j >= 0 && (s[j] == '(' || s[j] == ')')) j--;
      // '-' is operator
      if (j != -1 && (s[j] != '+' && s[j] != '-' && s[j] != '*')) {
        char top_op;
        while (!ops.empty() && ((top_op = ops.top())== '+' || top_op == '-' || top_op == '*')) {
          pop_op(ops, vals, t);
        }
        ops.push(s[i]);
        i++;
      }

      // '-' is negative sign
      else {
        int len = read_var(s, i);
        string num = s.substr(i, len);
        vals.push(stoi(num));
        i += len;
      }
    }

    // if it's '+'
    else if (s[i] == '+') {
      char top_op;
      while (!ops.empty() && ((top_op = ops.top()) == '+' || top_op == '-' || top_op == '*')) {
        pop_op(ops, vals, t);
      }
      ops.push(s[i]);
      i++;
    }

    // if it's '*'
    else if (s[i] == '*') {
      char top_op;
      while (!ops.empty() && (top_op = ops.top()) == '*') {
        pop_op(ops, vals, t);
      }
      ops.push(s[i]);
      i++;
    }

    // consume the next number
    else {
      int len = read_var(s, i);
      string num = s.substr(i, len);
      vals.push(stoi(num));
      i += len;
    }
  }

  while (!ops.empty()) {
    pop_op(ops, vals, t);
  }

  // Only one number left at the top of the value stack
  int remain = vals.top();
  return remain;

}

int main()
{
  table_t tbl;

  string line;
  // Assuming all inputs are legal
  while(getline(cin, line)) {
    string pattern1 = "PRINT";
    string pattern2 = "RESET";

    // Start with 'PRINT'
    if ( !line.compare(0, 5, pattern1) ) {
      string var = line.substr(6); // substring after "PRINT "
      string expr = var;
      // Expand, generate a string without any variable in the expression s
      int rc = expand(var, expr, tbl);
      if (rc == UNDEF) {
        cout << "UNDEF" << endl;
      } else {
        int val = evaluate(expr, tbl);
        tbl[var] = to_string(val);
        cout << val << endl;
      }
    } else if ( !line.compare(0, 5, pattern2) ) { // Start with 'RESET'
      tbl.clear();
    } else {
      size_t found = line.find(" := ");
      string var_name = line.substr(0, found);
      string assignment = line.substr(found+4);
      remove_space(assignment);
      pair<string, string> p(var_name, assignment);
      auto iter = tbl.find(var_name);
      if (iter == tbl.end()) {
        tbl.insert(p);
      } else {
        tbl[var_name] = assignment;
      }
    }

  }

  return 0;
}

