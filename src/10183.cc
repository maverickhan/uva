/*
  UVA online judge Problem P10183: How many fibs?
  Review of binary search
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "header/infint.hh"

using namespace std;

using bigint = InfInt;

int main()
{
  vector<bigint> fib = {1, 2};
  size_t fsize = 2;

  string s = "1";
  for (size_t i = 0; i < 100; ++i) {
    s.append(1, '0');
  }
  bigint ub(s);
  bigint lb("0");

  bigint k = fib[0] + fib[1];
  while (k < ub) {
    fib.push_back(k);
    fsize++;
    k = fib[fsize-1] + fib[fsize-2];
  }

  bigint a, b;
  while (cin >> a >> b) {
    if (a == 0 && b == 0) {
      break;
    }

    // upper_bound: first iterator greater than
    auto first_gt = upper_bound(fib.begin(), fib.end(), b);
    // lower_bound: first iterator no less than
    auto first_lt = lower_bound(fib.begin(), fib.end(), a);


    cout << first_gt - first_lt<< endl;
  }

  return 0;
}

