/*
  LeetCode 34: Search in a range
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int find_first_occurance_of(const vector<int>& v, int target)
{
  int n = int(v.size());
  int low = 0, high = n-1;
  int first_idx = -1;

  while (low <= high) {
    int mid = low + (high - low) / 2;
    if (v[mid] == target) {
      first_idx = mid;
      high = mid-1;
    } else if (v[mid] < target) {
      low = mid+1;
    } else {
      high = mid-1;
    }
  }

  return first_idx;
}

int find_last_occurance_of(const vector<int>& v, int target)
{
  int n = int(v.size());
  int low = 0, high = n-1;
  int last_idx = -1;

  while (low <= high) {
    int mid = low + (high - low) / 2;
    if (v[mid] == target) {
      last_idx = mid;
      low = mid+1;
    } else if (v[mid] < target) {
      low = mid+1;
    } else {
      high = mid-1;
    }
  }

  return last_idx;
}

int main()
{
  string line;
  getline(cin, line);
  stringstream ss(line);
  string num;
  vector<int> v;
  while(getline(ss, num, ' ')){
    v.push_back(stoi(num));
  }

  int k;
  cin >> k;
  cin.ignore();

  for (int i = 0; i < k; ++i) {
    int target;
    cin >> target;
    cin.ignore();

    int idx1, idx2;
    idx1 = find_first_occurance_of(v, target);
    if (idx1 == -1) {
      idx2 = -1;
    } else {
      idx2 = find_last_occurance_of(v, target);
    }

    cout << idx1 << ", " << idx2 << endl;
   }

  return 0;
}

