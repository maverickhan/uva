/*
  LeetCode 221: Maximal Square
  Build auxilary matrix
  S[i][j]: maximum sub-matrix with i,j being the bottom right element.
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  size_t m, n;
  cin >> m;
  cin.ignore();
  cin >> n;
  cin.ignore();

  vector<vector<int>> mat(m+1, vector<int>(n+1));
  for (size_t i = 1; i <= m; ++i) {
    string line;
    getline(cin, line);
    stringstream ss(line);
    string num;
    int j = 1;
    while (getline(ss, num, ' ')) {
      mat[i][j++] = stoi(num);
    }
  }

  vector<vector<int>> aux(m+1, vector<int>(n+1));
  int max_sq = 0;
  for (size_t i = 1; i <= n; ++i) {
    aux[1][i] = mat[1][i] == 1 ? 1 : 0;
    if (aux[1][i] == 1) max_sq = 1;
  }
  for (size_t i = 1; i <= m; ++i) {
    aux[i][1] = mat[i][1] == 1 ? 1 : 0;
    if (aux[i][1] == 1) max_sq = 1;
  }

  for (size_t i = 2; i <= m; ++i) {
    for (size_t j = 2; j <= n; ++j) {
      if (mat[i][j] == 0) {
        aux[i][j] = 0;
      } else {
        aux[i][j] = min(min(aux[i][j-1], aux[i-1][j]), aux[i-1][j-1]) + 1;
        max_sq = aux[i][j] > max_sq ? aux[i][j] : max_sq;
      }
    }
  }

  cout << "Maximum square submatrix includes: " << max_sq * max_sq << " elements." << endl;

  return 0;
}

