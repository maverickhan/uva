/*
  UVA online judge Problem: Little Bishop
  Key to this problem is to remember each taboo position
  can be double calculated, so keep a counter rather than
  a bool value to keep track of the update.
  First attempt: time limit exceeded.
  Corner case: k = 0, output should be one.

  Some comments from stackoverflow for backtracking problem:
   - Try finite domain search, where every bishop has a domain. When you place a bishop, you prune the domain for remaining bishops.
   - If you have n bishops and m places left, where m < n, you must backtrack.
   - Use DP, try solve n+1 bishops using solution from n bishops.
   - Exploit symmetry to reduce search space, here you have black/white symmetry and rotational/reflective symmetry.
   - Try to find a better representation, such as bit patterns.
   - Think about how to undo the move efficiently if you use a different representation.

   Here is the solution
   - Divide the board to two sets, Black and white, black set cannot attack white set.
   - D_k = Sum_{i=0}^{k} (B_{i} * W_{k-i})
   - Now let's calculate each set independently with recursion and backtrack.
   - While calculating, remember solution for B/W_{i} where i = 0 to k.

   To use a custom key type for unordered_map, one must specialize std::hash and equality to instantiate
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

// utility function to calculate if two positions are in attacking mode.
bool is_attackable(const int i, const int j, const int n)
{
  if (abs(i / n - j / n) == abs(i % n - j % n)) {
    return true;
  }

  return false;
}

// calculate non-attackable position
// Pick candidate positions from remaining places in black or white board
void find_non_attack_pos(const int n, const vector<int>& candidates, const vector<int>& board, vector<int>& new_candidate_set)
{
  // find maximum index in candidates
  int max_idx = -1;
  for (const auto& i : candidates) {
    max_idx = i > max_idx ? i : max_idx;
  }

  // find non-attack positions and form new candidate set
  for (size_t pos = 0; pos < board.size(); ++pos) {
    if (board[pos] < max_idx) continue;
    else {
      // check if board[pos] is threatening any pos in candidates
      bool no_threat = true;
      for (size_t j = 0; j < candidates.size(); ++j) {
        if (is_attackable(board[pos], candidates[j], n)) {
          no_threat = false;
          break;
        }
      }
      if (no_threat) {
        new_candidate_set.push_back(board[pos]);
      }
    }
  }
}

// recursively count solutions and backtrack
// count -- number of bishops placed on board
// k -- total number of bishops
// n -- size of the board
// candidates -- occupied posisions so far
// board -- board posisions
// sol -- sol[i] represents number of solutions with i bishops
void recurr_count(const int n, size_t count, size_t k, vector<int>& candidates, const vector<int>& board, vector<size_t>& sol)
{
  if (count > k)
    return ;

  vector<int> new_candidate_set;
  find_non_attack_pos(n, candidates, board, new_candidate_set);

  for (const auto& i : new_candidate_set) {
    candidates.push_back(i);
    recurr_count(n, count+1, k, candidates, board, sol);
    sol[count]++;
    // backtrack
    candidates.pop_back();
  }
}

int main()
{
  int n, k;

  while (cin >> n >> k) {
    if (n == 0) {
      break;
    }

    if (k == 0) {
      cout << "1" << endl;
      continue;
    }

    uint64_t num_solution = 0;

    // initialize black and white half of the board.
    vector<int> black;
    vector<int> white;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0)) {
          black.push_back(i * n + j);
        } else {
          white.push_back(i * n + j);
        }
      }
    }

    vector<size_t> B(k+1, 0);
    vector<size_t> W(k+1, 0);

    B[0] = 1;
    W[0] = 1;

    size_t c = 1;
    vector<int> b_candidates;
    for (size_t i = 0; i < black.size(); ++i) {
      b_candidates.push_back(black[i]);
      recurr_count(n, c+1, k, b_candidates, black, B);
      B[c]++;

      // backtrack
      b_candidates.pop_back();
    }

    // no symmetry, we have to calculate white half of the board.
    if (n % 2 != 0) {
      c = 1;
      vector<int> w_candidates;
      for (size_t i = 0; i < white.size(); ++i) {
        w_candidates.push_back(white[i]);
        recurr_count(n, c+1, k, w_candidates, white, W);
        W[c]++;

        // backtrack
        w_candidates.pop_back();
      }
    } else {
      for (size_t i = 0; i <= k; ++i) {
        W[i] = B[i];
      }
    }

    // debug
    // for (size_t i = 0; i <= k; ++i) {
    //   cout << "B[" << i << "]: " << B[i] << endl;
    //   cout << "W[" << k-i << "]: " << W[k-i] << endl;
    // }

    // Calculate final number
    for (size_t i = 0; i <= k; ++i) {
      num_solution += B[i] * W[k-i];
    }

    cout << num_solution << endl;

  }

  return 0;
}

