/*
  LeetCode 10: Regular Expression Matching
  Note that ab <-> .* because . matches a|b
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

bool isMatchHelper(const string& s, const string& p, size_t s_begin, size_t p_begin)
{
  size_t m = s.size();
  size_t n = p.size();

  if (s_begin == m && p_begin == n) {
    return true;
  }

  if (s_begin == m || p_begin == n) {
    return false;
  }

  // regular case, not followed by '*'
  if ((p[p_begin] != '*' && p_begin == n-1) ||
      (p[p_begin] != '*' && p[p_begin+1] != '*')) {
    if (p[p_begin] == '.' || p[p_begin] == s[s_begin]) {
      return isMatchHelper(s, p, s_begin+1, p_begin+1);
    } else {
      return false;
    }
  } else if (p[p_begin] != '*' && p_begin < n-1 && p[p_begin+1] == '*') { 
    // regular char or '.' followed by '*'
    if (p[p_begin] == s[s_begin] || p[p_begin] == '.') {
      int k = 1, j = s_begin+1;
      // look forward, find how many chars equal to current one
      while(j < n) {
        if (s[j] == s[s_begin] || p[p_begin] == '.') {
          k++;
        }
        j++;
      }
      // ignore this group
      bool trymatch = false;
      // or "eat" i chars in s
      for (int i = 0; i <= k; ++i) {
        trymatch |= isMatchHelper(s, p, s_begin+i, p_begin+2);
        if (trymatch) return true;
      }
    } else {
      return isMatchHelper(s, p, s_begin, p_begin+2);
    }
  }

  return false;
}

bool isMatch(const string& s, const string& p)
{
  if (s.empty() || p.empty()) {
    return false;
  }

  return isMatchHelper(s, p, 0, 0);
}

int main()
{
  string line;
  while (getline(cin, line)) {
    stringstream ss(line);
    string s, p;
    getline(ss, s, ' ');
    getline(ss, p, ' ');

    bool rlt = isMatch(s, p);
    string output = rlt ? "true" : "false";

    cout << output << endl;
  }

  return 0;
}

