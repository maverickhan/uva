/*
  UVA online judge Problem P10082: WERTYU
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

using map_t = unordered_map<char, char>;

int main()
{
  char keys[47] = {'`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=',
                   'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\\',
                   'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\'',
                   'Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', '/'};

  map_t keymap;
  for (size_t i = 1; i < 47; ++i) {
    keymap.insert(pair<char, char>(keys[i], keys[i-1]));
  }

  string line;
  while(getline(cin, line)) {
    for (size_t i = 0; i < line.length(); ++i) {
      if (line[i] == ' ') cout << ' ';
      else cout << keymap[line[i]];
    }
    cout << endl;
  }

  return 0;
}