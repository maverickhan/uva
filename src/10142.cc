/*
  UVA online judge Problem p10142: Austrilian Voting
  - Compare must take care of type coherence
    Never compare unsigned with signed integer.
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

using cmap_t = unordered_map<int, string>;
using vmap_t = unordered_map<int, int>;
using vote_t = vector<vector<int>>;

void read_candidate(cmap_t& cs, size_t n)
{
  string line;
  for (size_t i = 0; i < n; ++i) {
    getline(cin, line);
    cs.insert(pair<int, string>(i+1, line));
  }
}

void read_votes(vote_t& v)
{
  string line;
  getline(cin, line);
  while (!line.empty()) {
    vector<int> u;
    string cnt;
    stringstream ss(line);
    while(getline(ss, cnt, ' ')) {
      u.push_back(stoi(cnt));
    }
    v.push_back(u);
    getline(cin, line);
  }
}

// Corner case, count should also consider candidates
// with zero votes.
void count_votes(const cmap_t& c,
                 vmap_t& vmap,
                 vote_t& v)
{
  for (auto i : v) {
    for (auto j : i) {
      auto it1 = c.find(j);
      if (it1 != c.end()) {
        auto it2 = vmap.find(j);
        if (it2 != vmap.end()) {
          vmap[j]++;
        } else {
          vmap.insert(pair<int, int>(j, 1));
        }
        // found votes for preferred candidate who has
        // not been eliminated yet, break out the inner
        // loop.
        break;
      }
    }
  }

  for (auto i : c) {
    int id = i.first;
    auto it = vmap.find(id);
    if (it == vmap.end()) {
      vmap.insert(pair<int, int>(id, 0));
    }
  }
}

int find_winner(cmap_t& c,
                const vmap_t& vmap,
                const int num_votes)
{
  if (vmap.empty()) {
    return 0;
  }

  int max = INT_MIN;
  int min = INT_MAX;
  int winner_id = -1;
  vector<int> losers;

  int vote, temp;
  size_t cnt = 0;
  bool is_tied = true;
  bool skip_check = false;
  for (auto i : vmap) {
    vote = i.second;

    // Check for tie while scanning votes.
    if (cnt == 0) {
      temp = vote;
    } else if (!skip_check && cnt > 0) {
      if (temp != vote) {
        is_tied = false;
        skip_check = true;
      }
    }

    // update max vote
    if (vote > max) {
      max = vote;
      winner_id = i.first;
    }
    min = vote < min ? vote : min;
    cnt++;
  }

  if (is_tied) {
    // cout << "We have a tie!" << endl;
    return 0;
  }

  // we have a winner
  if (max > num_votes / 2) {
    return winner_id;
  }

  // else we have to eliminate losers
  // Note that there might be multiple losers
  for (auto i : vmap) {
    vote = i.second;
    if (vote == min) {
      losers.push_back(i.first);
    }
  }

  for (auto i : losers) {
    auto it = c.find(i);
    if (it != c.end()) {
      c.erase(it);
      // cout << it->first << " out!" << endl;
    }
  }

  return -1;
}

int main()
{
  size_t num_test;
  cin >> num_test;
  cin.ignore();

  while (num_test > 0) {
    size_t n;
    cin >> n;
    cin.ignore();
    // candidate_id: candidate_name
    cmap_t candidates;
    vote_t v;
    read_candidate(candidates, n);
    read_votes(v);

    // candidate_id: candidate_votes
    vmap_t votes;
    // -1 -- no winner for this round
    // 0  -- all remaining candidates tied
    // >0 -- winner receive > 50% votes
    int winner_id = -1;
    while (winner_id < 0) {
      votes.clear();
      count_votes(candidates, votes, v);
      // for (auto i : votes) {
      //   cout << i.first << " got " << i.second << endl;
      // }
      winner_id = find_winner(candidates, votes, v.size());
    }

    if (winner_id > 0) {
      cout << candidates[winner_id] << endl;
    } else {
      for (size_t i = 1; i <= n; ++i) {
        cmap_t::const_iterator it;
        if ((it = candidates.find(i)) != candidates.end()) {
          cout << candidates[i] << endl;
        }
      }
    }

    // blank line between test case output
    if (num_test > 1) {
      cout << endl;
    }
    num_test--;
  }

  return 0;
}
