/*
  UVA online judge Problem :
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

void print_image(const vector<vector<int>>& image)
{
  for (const auto& i:image) {
    for (const auto& j:i) {
      cout << j << ' ';
    }
    cout << endl;
  }
}

// in-place rotate, recursive update
void rotate_image(vector<vector<int>>& img, int low, int high)
{
  if (low >= high) return ;
  vector<int> tmp(high-low+1);

  // save row 'low' to tmp
  for (size_t i = low; i <= high; ++i) {
    tmp[i] = img[low][i];
  }

  // copy col 'low' to row 'low'
  for (size_t i = low, j = high; i <= high; ++i, --j) {
     img[low][j] = img[i][low];
  }
  // copy row 'high' to col 'low'
  for (size_t i = low; i <= high; ++i) {
    img[i][low] = img[high][i];
  }
  // copy col 'high' to row 'high'
  for (size_t i = low, j = high; i <= high; ++i, --j) {
    img[high][i] = img[j][high];
  }
  // finally, copy tmp to row 'high'
  for (size_t i = low; i <= high; ++i) {
    img[i][high] = tmp[i];
  }

  rotate_image(img, low+1, high-1);
}

int main()
{
  size_t n;
  cin >> n;
  cin.ignore();
  vector<vector<int>> image;
  for (size_t i = 0; i < n; ++i) {
    string line;
    getline(cin, line);
    stringstream ss(line);
    string num;
    vector<int> v;
    while(getline(ss, num, ' ')) {
      v.push_back(stoi(num));
    }
    image.push_back(v);
  }

  cout << "Before Rotate: " << endl;
  print_image(image);

  rotate_image(image, 0, n-1);

  cout << "After Rotate: " << endl;
  print_image(image);

  return 0;
}

