/*
  UVA online judge Problem P10267: Graphical Editor
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

using table_t = vector<vector<char>>;
using insct_t = vector<string>;
using coord_t = pair<size_t, size_t>;

bool is_number(const string& s)
{
  for (auto it = s.begin(); it != s.end(); ++it) {
    if (!isdigit(*it)) {
      return false;
    }
  }

  return true;
}

void process_line(const string& line, vector<string>& v)
{
  string operands = line.substr(2);
  stringstream ss(operands);
  string op;
  while (getline(ss, op, ' ')) {
    v.push_back(op);
  }
}

void fill_group(table_t& tbl, vector<coord_t>& p, char color)
{
  for (auto point : p) {
    size_t i = point.first;
    size_t j = point.second;

    tbl[i][j] = color;
  }
}

void output_table(const table_t& tbl)
{
  for (const auto i : tbl) {
    for (const auto j : i) {
      cout << j;
    }
    cout << endl;
  }
}

void process(const insct_t& inst, table_t& tbl)
{
  // table not yet constructed
  if (tbl.empty()) {
    return ;
  }

  size_t width = tbl.size();
  size_t height = tbl[0].size();

  for (auto str : inst) {
    // invalid command
    if (str[0] != 'C' && str[1] != ' ') {
      continue;
    }

    char command = str[0];
    vector<string> oplist;
    size_t c1, c2;
    size_t r1, r2;
    size_t lb, ub;
    size_t a, b, c, d;
    char color;
    vector<coord_t> to_be_filled;

    switch(command) {
      // clear table
      case 'C':
        for (auto &i : tbl) {
          for (auto &j : i) {
            j = 'O';
          }
        }
        break;
      case 'L':
        process_line(str, oplist);

        if (!is_number(oplist[0]) ||
            !is_number(oplist[1])) {
          continue;
        }
        a = stoi(oplist[0]);
        b = stoi(oplist[1]);
        if ((a < 1 || a > height+1) ||
            (b < 1 || b > width+1)
           ) {
          continue;
        }

        c1 = a - 1;
        r1 = b - 1;
        color = oplist[2][0];

        tbl[r1][c1] = color;
        break;

      case 'V':
        process_line(str, oplist);

        if (!is_number(oplist[0]) ||
            !is_number(oplist[1]) ||
            !is_number(oplist[2])) {
          continue;
        }
        a = stoi(oplist[0]);
        b = stoi(oplist[1]);
        c = stoi(oplist[2]);
        if ((a < 1 || a > height+1) ||
            (b < 1 || b > width+1) ||
            (c < 1 || c > width+1)
           ) {
          continue;
        }
        c1 = a - 1;
        r1 = b - 1;
        r2 = c - 1;
        color = oplist[3][0];
        if (r1 > r2) {
          lb = r2;
          ub = r1;
        } else {
          lb = r1;
          ub = r2;
        }

        for (size_t i = lb; i <= ub; ++i) {
          tbl[i][c1] = color;
        }
        break;

      case 'H':
        process_line(str, oplist);

        if (!is_number(oplist[0]) ||
            !is_number(oplist[1]) ||
            !is_number(oplist[2])) {
          continue;
        }
        a = stoi(oplist[0]);
        b = stoi(oplist[1]);
        c = stoi(oplist[2]);
        if ((a < 1 || a > height+1) ||
            (b < 1 || b > height+1) ||
            (c < 1 || c > width+1)
           ) {
          continue;
        }

        c1 = a - 1;
        c2 = b - 1;
        r1 = c - 1;
        color = oplist[3][0];
        if (c1 > c2) {
          lb = c2;
          ub = c1;
        } else {
          lb = c1;
          ub = c2;
        }

        for (size_t i = lb; i <= ub; ++i) {
          tbl[r1][i] = color;
        }
        break;

      case 'K':
        process_line(str, oplist);

        if (!is_number(oplist[0]) ||
            !is_number(oplist[1]) ||
            !is_number(oplist[2]) ||
            !is_number(oplist[3])) {
          continue;
        }
        a = stoi(oplist[0]);
        b = stoi(oplist[1]);
        c = stoi(oplist[2]);
        d = stoi(oplist[3]);
        if ((a < 1 || a > height+1) ||
            (b < 1 || b > height+1) ||
            (c < 1 || c > width+1) ||
            (d < 1 || d > width+1)
           ) {
          continue;
        }
        c1 = a - 1;
        r1 = b - 1;
        c2 = c - 1;
        r2 = d - 1;

        color = oplist[4][0];

        for (size_t i = r1; i <= r2; ++i) {
          for (size_t j = c1; j <= c2; ++j) {
            tbl[i][j] = color;
          }
        }
        break;

      case 'F':
        process_line(str, oplist);

        if (!is_number(oplist[0]) ||
            !is_number(oplist[1])) {
          continue;
        }
        a = stoi(oplist[0]);
        b = stoi(oplist[1]);
        if ((a < 1 || a > height+1) ||
            (b < 1 || b > width+1)
           ) {
          continue;
        }

        c1 = a - 1;
        r1 = b - 1;
        color = oplist[2][0];
        to_be_filled.push_back(coord_t(r1, c1));

        for (size_t i = 0; i < width; ++i) {
          for (size_t j = 0; j < height; ++j) {
            if (r1 == i && c1 == j) {
              ;
            } else if (r1 == i || c1 == j) {
              if (tbl[r1][c1] == tbl[i][j]) {
                to_be_filled.push_back(coord_t(i, j));
              }
            } else {
              if ((tbl[i][j] == tbl[i][c1] &&
                   tbl[i][c1] == tbl[r1][c1]) ||
                  (tbl[i][j] == tbl[r1][j] &&
                   tbl[r1][j] == tbl[r1][c1])
                  ) {
                to_be_filled.push_back(coord_t(i, j));
              }
            }
          }
        }

        fill_group(tbl, to_be_filled, color);
        break;

      default:
        break;
    }
  }
}

int main()
{
  size_t m, n;

  bool exit = false;
  string line;
  insct_t inst;
  table_t tbl;

  while (!exit) {
    getline(cin, line);
    inst.push_back(line);
    if (line == "X") {
      exit = true;
    } else if (line[0] == 'I' && line[1] == ' ') {
      // Constuct a new table as the current table.
      string dimension = line.substr(2);
      stringstream ss(dimension);
      ss >> m >> n;
      tbl = table_t(n, vector<char>(m, 'O'));
    } else if (line[0] == 'S' && line[1] == ' ') {
      string filename = line.substr(2);
      cout << filename << endl;
      process(inst, tbl);
      output_table(tbl);
      inst.clear();
    }
  }

  return 0;
}