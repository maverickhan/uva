/*
  UVA online judge Problem P10038: Jolly Jumper
  - Data Structure to use: unordered_set
  - Assumption implies no duplicate number in the
  - input sequence.
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  string line;
  while (getline(cin, line)) {
    string num;
    stringstream ss(line);
    unordered_set<int> diff;

    getline(ss, num, ' ');
    size_t n = stoi(num);

    // in case it's a single number
    if (n == 1) {
      cout << "Jolly" << endl;
      continue;
    }

    getline(ss, num, ' ');
    int old_val = stoi(num);

    while (getline(ss, num, ' ')) {
      int new_val = stoi(num);
      int d = abs(new_val - old_val);
      if (d < n && d > 0) {
        diff.insert(d);
      }
      old_val = new_val;
    }

    if (diff.size() == n-1) {
      cout << "Jolly" << endl;
    } else {
      cout << "Not jolly" << endl;
    }

  }

  return 0;
}