#pragma once

#include <iostream>
#include <vector>

using namespace std;

namespace uva {

template <typename T>
void print_number(const T n) {
  cout << n << endl;
}

template <typename T>
void print_vector(const vector<T>& v) {
  for (auto i : v) {
    cout << i << " ";
  }
  cout << endl;
}

template <typename T>
void print_matrix(const vector<vector<T>>& v) {
  for (auto i : v) {
    for (auto j : i) {
      cout << j << " ";
    }
    cout << endl;
  }
}

}; // namespace uva