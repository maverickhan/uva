#pragma once

#include <iostream>
#include <memory>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

struct vertex {
	// an edge from vertex v is defined as an end vertex with certain associated cost
	typedef std::pair<int, std::shared_ptr<vertex>> edge;

	std::string id;
	std::vector<edge> neighbor;
	// vertex is not the only owner for parent
	std::shared_ptr<vertex> parent;

	vertex(){}
	vertex(const std::string& n) : id(n) {}

	vertex(const vertex& rhs) {
		id = rhs.id;
		neighbor = rhs.neighbor;
		parent = rhs.parent;
	}

	vertex& operator=(const vertex& rhs) = delete;
};

struct graph {
	// use id to index vertex, which comprimises the adjacency list
	typedef std::unordered_map<std::string, std::shared_ptr<vertex>> vlist;

	vlist	 adj; 	
	bool 	 is_directed;
	size_t nvertices;
	size_t nedges;

	graph(){}
	graph(bool directed) : is_directed(directed) {} 

	void add_vertex(const std::string& s) {
		if (adj.find(s) == adj.end()) {
			adj[s] = std::make_shared<vertex>(vertex(s));
			nvertices++;
		}
	}

	void add_edge(const std::string& from, const std::string& to, int cost) {
		if (from.empty() || to.empty()) return ;
		// Add two vertices if they are not part of the graph yet.
		if (adj.find(from) == adj.end()) {
			add_vertex(from);
		}
		if (adj.find(to) == adj.end()) {
			add_vertex(to);
		}

		(adj[from]->neighbor).push_back(std::make_pair(cost, std::make_shared<vertex>(vertex(to))));
		nedges++;
	}
};

template <typename VERTEX_FUNC>
void bfs(const std::string& source, graph& g, VERTEX_FUNC&& vf)
{
	enum state {
		UNDISCOVERED = 0,
		DISCOVERED   = 1,
		PROCESSED		 = 2
	};

	// initialize graph node states
	typedef std::unordered_map<std::string, state> state_map;

	if (source.empty() || g.adj.find(source) == g.adj.end()) {
		std::cout << "Invalid source vertex for BFS." << std::endl;
		return ;
	}

	state_map sm;
	for (auto it = g.adj.begin(); it != g.adj.end(); ++it) {
		sm[it->first] = UNDISCOVERED;
	}

	std::deque<std::string> dq;
	sm[source] = DISCOVERED;
	dq.push_back(source);

	while (!dq.empty()) {
		auto u = (g.adj)[dq.front()];
		for (const auto& i : u->neighbor) {
			auto v = i.second;
			if (sm[v->id] == UNDISCOVERED) {
				v->parent = u;
				dq.push_back(v->id);
				sm[v->id] = DISCOVERED;
			}
		}
		
		vf(u);
		dq.pop_front();
		sm[u->id] = PROCESSED;
	}
}
