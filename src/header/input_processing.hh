#pragma once

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

namespace uva {

// Read a single integer from std::cin
void
read_number(int& n)
{
  string line;
  getline(cin, line);
  n = stoi(line);
}

// Read a single floating point from std::cin
void
read_number(float& n)
{
  string line;
  getline(cin, line);
  n = stof(line);
}

// Read a integer vector from std::cin
void
read_vector(vector<int>& v)
{
  string line, num;
  getline(cin, line);
  stringstream ss(line);
  while (getline(ss, num, ' ')) {
    v.push_back(stoi(num));
  }
}

}; // namespace uva