/*
  UVA online judge Problem P10149: Yahtzee
  bitset rightmost bit is position 0
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

#define ROUND 13
#define NUMD  5

using dice_t = vector<vector<size_t>>;
using category = bitset<ROUND>;

// debug input processing
void output(const dice_t& d)
{
	for (const auto& i : d) {
		for (const auto& j : i) {
			cout << j << ",";
		}
		cout << endl;
	}
	cout << endl;
}

void output_category(const size_t i, const size_t pos, const size_t score)
{
	string x = "";
	if (pos == 0) x = "ones";
	else if (pos == 1) x = "twos";
	else if (pos == 2) x = "threes";
	else if (pos == 3) x = "fours";
	else if (pos == 4) x = "fives";
	else if (pos == 5) x = "sixes";
	else if (pos == 6) x = "chance";
	else if (pos == 7) x = "three of a kind";
	else if (pos == 8) x = "four of a kind";
	else if (pos == 9) x = "five of a kind";
	else if (pos == 10) x = "short straight";
	else if (pos == 11) x = "long straight";
	else if (pos == 12) x = "full house";
	else x = "error!";

	cout << i << ": " << x << " - score: " << score << endl;
}

void try_match(const vector<size_t>& v,
	             unordered_map<size_t, size_t>& candidates,
	             const category& c)
{
	unordered_map<size_t, size_t> dice_count({{1, 0}, {2, 0}, {3, 0},
		                                        {4, 0}, {5, 0}, {6, 0}});
	for (const auto& i : v) {
		dice_count[i]++;
	}

	size_t ones = dice_count[1];
	size_t twos = dice_count[2] * 2;
	size_t threes = dice_count[3] * 3;
	size_t fours = dice_count[4] * 4;
	size_t fives = dice_count[5] * 5;
	size_t sixes = dice_count[6] * 6;
	size_t chance = ones + twos + threes + fours + fives + sixes;

	bool has_two = false, has_three = false, has_four = false, has_five = false;
	bool is_three_kind, is_four_kind, is_five_kind, full_house;
	bool short_straight = false;
	bool long_straight  = false;

	for (size_t i = 1; i <= 6; ++i) {
		if (dice_count[i] == 2) {
			has_two = true;
		} else if (dice_count[i] == 3) {
			has_three = true;
		} else if (dice_count[i] == 4) {
			has_four = true;
		} else if (dice_count[i] == 5) {
			has_five = true;
		}
	}

	is_three_kind = has_three || has_four || has_five;
	is_four_kind = has_four || has_five;
	is_five_kind = has_five;
	full_house = has_two && has_three;
	long_straight = (!has_two) && (!has_three) && (!has_four) && (!has_five);
	short_straight = (has_two && (dice_count[3] != 0) && (dice_count[4] != 0) && (!has_three) && (!has_four) && (!has_five))
	                 || long_straight;

	if (!c.test(0)) //ones
		candidates.insert(make_pair(0, ones));
	if (!c.test(1)) // twos
		candidates.insert(make_pair(1, twos));
	if (!c.test(2)) // threes
		candidates.insert(make_pair(2, threes));
	if (!c.test(3)) // fours
		candidates.insert(make_pair(3, fours));
	if (!c.test(4)) // fives
		candidates.insert(make_pair(4, fives));
	if (!c.test(5)) // sixes
		candidates.insert(make_pair(5, sixes));
	if (!c.test(6)) // chance
		candidates.insert(make_pair(6, chance));
	if (!c.test(7) && is_three_kind) // three of a kind
		candidates.insert(make_pair(7, chance));
	if (!c.test(8) && is_four_kind) // four of a kind
		candidates.insert(make_pair(8, chance));
	if (!c.test(9) && is_five_kind) // five of a kind
		candidates.insert(make_pair(9, 50));
	if (!c.test(10) && short_straight) // short straight
		candidates.insert(make_pair(10, 25));
	if (!c.test(11) && long_straight) // long straight
		candidates.insert(make_pair(11, 35));
	if (!c.test(12) && full_house) // full house
		candidates.insert(make_pair(12, 40));
}

// backtrack to find the max score for a game
void find_max(dice_t& d, size_t i, category& c, size_t& first6, size_t& sum,
	            size_t& max, vector<size_t>& score, vector<size_t>& max_score)
{
	// cout << "Call find_max: " << i << endl;

	if (i == ROUND) {
		if (first6 >= 63) {
			sum += 35;
		}

		if (sum > max) {
			max = sum;
			max_score.assign(score.begin(), score.end());
		}

		return ;
	}

	// key - category id, value - score
	unordered_map<size_t, size_t> candidates;
	try_match(d[i], candidates, c);
	if (candidates.empty()) {
		// cout << i << ": no match!\n";
		score.push_back(0);
		find_max(d, i+1, c, first6, sum, max, score, max_score);
		score.pop_back();
	} else{
		for (auto it = candidates.begin(); it != candidates.end(); ++it) {
			c.set(it->first);
			// output_category(i, it->first, it->second);
			score.push_back(it->second);
			sum += it->second;
			if (it->first < 6) {
				first6 += it->second;
			}

			find_max(d, i+1, c, first6, sum, max, score, max_score);

			// did not work, backtrack
			c.reset(it->first);
			score.pop_back();
			sum -= it->second;
			if (it->first < 6) {
				first6 -= it->second;
			}
		}
	}
}

int main()
{
	dice_t d(ROUND, vector<size_t>(NUMD, 0));
	category c;
	size_t rd = 1;
	string line;
	size_t max = 0;
	size_t first6 = 0;
	size_t sum = 0;
	vector<size_t> score;
	vector<size_t> max_score;

	while (getline(cin, line)) {
		stringstream ss(line);
		string s;
		size_t i = 0;
		while(getline(ss, s, ' ')) {
			d[rd-1][i++] = stoi(s);
		}

		// advance round
		if (rd == ROUND) {
			output(d);

			find_max(d, 0, c, first6, sum, max, score, max_score);

			// output for the game.
			for (const auto& i : max_score) {
				cout << i << " ";
			}
			if (first6 >= 63) {
				cout << "35 ";
			} else {
				cout << "0 ";
			}
			cout << max << endl;

			// reset everything
			c.reset();
			max = 0;
			first6 = 0;
			sum = 0;
			rd = 0;
			score.clear();
			max_score.clear();
		}
		rd++;
	}

	return 0;
}