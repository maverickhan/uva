/*
  UVA online judge Problem P10132: File Fragmentation
  Intersection of two files, better use ordered_set
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();

  string line;
  getline(cin, line);
  vector<string> v;
  size_t N = 0;
  vector<string> pattern;

  for (size_t i = 0; i < ntest; ++i) {
    v.clear();
    pattern.clear();
    while (getline(cin, line)) {
      if (line.empty()) break;
      else {
        v.push_back(line);
        N += line.size();
      }
    }

    set<string> curr_set;

    // N is the length of the pattern string
    size_t num_file = v.size() / 2;
    N /= num_file;
    sort(v.begin(), v.end(), [](const string& s1, const string& s2){
      return s1.size() < s2.size();
    });

    vector<string> v1(v.begin(), v.begin()+num_file);
    vector<string> v2(v.begin()+num_file, v.end());
    reverse(v2.begin(), v2.end());

    vector<string>::iterator it1, it2;
    int sz = -1;
    for (it1 = v1.begin(), it2 = v2.begin();
         it1 != v1.end();
         ++it1, ++it2) {

      if (it1->size() != sz) {
        curr_set.clear();
        sz = it1->size();
      }

      // insert to curr_set, forward and backward
      string s1 = "";
      s1.append(*it1);
      s1.append(*it2);
      curr_set.insert(s1);
      string s2 = "";
      s2.append(*it2);
      s2.append(*it1);
      curr_set.insert(s2);

      // first iteration
      if (pattern.empty()) {
        copy(curr_set.begin(), curr_set.end(), back_inserter(pattern));
      } else if (it1 == v1.end()-1 || (it1+1)->size() != sz){
        set<string> tmp(pattern.begin(), pattern.end());
        pattern.clear();

        // calculate intersection of
        set_intersection(curr_set.begin(), curr_set.end(),
                         tmp.begin(), tmp.end(),
                         back_inserter(pattern));
      }
    }

    // output
    if (!pattern.empty()) {
      sort(pattern.begin(), pattern.end());
      cout << pattern[0] << endl;
    }
    if (i != ntest-1) {
      cout << endl;
    }
  }

  return 0;
}
