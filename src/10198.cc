/*
  UVA online judge Problem P10198: Counting
  Dynamic Programming
  C[N] = C[N-1] + C[N-2] + C[N-3] + C[N-1] (1 & 4)
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "header/infint.hh"

using namespace std;

#define N 1001

int main()
{
  bigint c[N];
  c[1] = 2;
  c[2] = 5;
  c[3] = 13;

  for (size_t i = 4; i < N; ++i) {
    c[i] = c[i-1] + c[i-2] + c[i-3] + c[i-1];
  }

  int n;
  while (cin >> n) {
    cout << c[n] << endl;
  }

  return 0;
}

