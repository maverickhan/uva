/*
  UVA online judge Problem P848: Fmt
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#define LW 72

using namespace std;

int main()
{
  // don't skip whitespace while reading
  cin >> noskipws;

  istreambuf_iterator<char> begin(cin);
  istreambuf_iterator<char> end;
  string s(begin, end);
  int sz = s.size();

  string paragraph;
  int pb = 0;
  int pe;
  for (int i = 0; i < sz; ++i) {
    if ((i == sz - 1) || (s[i] == '\n' && ((s[i+1] == '\n') || (s[i+1] == ' ')))) {
      pe = i;
      // we extract a paragraph here
      // cout << pb << " - " << pe << endl;
      paragraph = s.substr(pb, pe-pb+1);
      if (paragraph == "\n") {
        cout << paragraph << endl;
        i+=2;
        pb = i;
        continue;
      } else {
        // replace every '\n' to space, except for the last one
        for (int j = 0; j < paragraph.size()-1; ++j) {
          if (paragraph[j] == '\n') {
            paragraph[j] = ' ';
          }
        }
        cout << paragraph << endl;

        // now start to format the paragraph
        int lb = 0;
        int le = lb + LW;
        while (le < paragraph.size()) {
          // check if the whole line has one or more spaces
          bool has_space = false;
          for (int k = lb; k < le; ++k) {
            if (paragraph[k] == ' ') {
              has_space = true;
            }
          }

          if (has_space) {
            if (paragraph[le] == ' ') {
              paragraph[le] = '\n';
              lb = le + 1;
            } else {
              // insert line break at the previous blank space
              // look back
              for(int k = le-1; k >= lb; --k) {
                if (paragraph[k] == ' ') {
                  paragraph[k] = '\n';
                  lb = k + 1;
                  break;
                }
              }
            }
          } else {
            for (int k = le; k < paragraph.size(); ++k) {
              if (paragraph[k] == ' ') {
                paragraph[k] = '\n';
                lb = k + 1;
                break;
              }
            }
          }

          le = lb + LW;
        }

        cout << paragraph << endl;
        i += 2;
        pb = i;
      }

    }
  }

  return 0;
}
