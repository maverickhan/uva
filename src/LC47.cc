/*
  LeetCode 47: Permutation II
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

void permutation(unordered_map<int, int>& m,
                 vector<vector<int>>& perms,
                 vector<int>& v)
{
  bool is_leaf_case = true;
  for (auto it = m.begin(); it != m.end(); ++it) {
    if (it->second > 0) {
      is_leaf_case = false;
      v.push_back(it->first);
      (it->second)--;
      permutation(m, perms, v);
      v.pop_back();
      (it->second)++;
    }
  }

  if (is_leaf_case) {
    vector<int> vcopy(v);
    perms.push_back(vcopy);
  }
}

int main()
{
  string line;
  getline(cin, line);

  stringstream ss(line);
  string num;
  unordered_map<int, int> m;
  while(getline(ss, num, ' ')) {
    int k = stoi(num);
    if (m.find(k) == m.end()) {
      m.insert(make_pair(k, 1));
    } else {
      m[k]++;
    }
  }

  vector<vector<int>> perms;

  for (auto it = m.begin(); it != m.end(); ++it) {
    vector<int> perm = vector<int>(1, it->first);
    (it->second)--;
    permutation(m, perms, perm);
    (it->second)++;
  }

  // print out results
  for (const auto& i : perms) {
    for (const auto& j : i) {
      cout << j << ' ';
    }
    cout << endl;
  }

  return 0;
}

