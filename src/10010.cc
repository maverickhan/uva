/*
  UVA online judge Problem P10010: Finding Woldorf
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

using grid_t = vector<vector<char>>;
using dict_t = vector<string>;

void w2l(string& s)
{
  for (size_t i = 0; i < s.size(); ++i) {
    s[i] = tolower(s[i]);
  }
}

// check if string s exists in g[r][c]'s eight directions
bool checkvhd(const string& s, grid_t& g,
              int r, int c,
              int m, int n)
{
  string hs1 = "", hs2 = "";           // horizontal strings
  string vs1 = "", vs2 = "";           // vertical strings
  string ds1 = "", ds2 = "", ds3 = "", ds4 = ""; // diagonal strings
  int size = static_cast<int>(s.size());
  int lb = c-size+1;
  int rb = c+size-1;
  int uub = r-size+1;
  int llb = r+size-1;

  if (lb >= 0) {
    for (int i = c; i >= lb; --i) {
      hs1.append(1, g[r][i]);
    }
    if (hs1.compare(s) == 0) return true;
  }
  if (rb < n) {
    for (int i = c; i <= rb; ++i) {
      hs2.append(1, g[r][i]);
    }
    if (hs2.compare(s) == 0) return true;
  }
  if (uub >= 0) {
    for (int i = r; i >= uub; --i) {
      vs1.append(1, g[i][c]);
    }
    if (vs1.compare(s) == 0) return true;
  }
  if (llb < m) {
    for (int i = r; i <= llb; ++i) {
      vs2.append(1, g[i][c]);
    }
    if (vs2.compare(s) == 0) return true;
  }
  if (lb >= 0 && uub >= 0){
    for (int i = r, j = c; i >= uub; --i, --j) {
      ds1.append(1, g[i][j]);
    }
    if (ds1.compare(s) == 0) return true;
  }
  if (rb < n && uub >= 0) {
    for (int i = r, j = c; i >= uub; --i, ++j) {
      ds2.append(1, g[i][j]);
    }
    if (ds2.compare(s) == 0) return true;
  }
  if (lb >= 0 && llb < m) {
    for (int i = r, j = c; i <= llb; ++i, --j) {
      ds3.append(1, g[i][j]);
    }
    if (ds3.compare(s) == 0) return true;
  }
  if (rb < n && llb < m) {
    for (int i = r, j = c; i <= llb; ++i, ++j) {
      ds4.append(1, g[i][j]);
    }
    if (ds4.compare(s) == 0) return true;
  }

  return false;
}

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();
  string line;

  // ignore the first empty line
  getline(cin, line);

  for (size_t i = 0; i < ntest; ++i) {
    size_t m, n, k;

    cin >> m >> n;
    cin.ignore();

    // read in char grid
    grid_t grid = vector<vector<char>>(m, vector<char>(n));
    string line;
    for (size_t r = 0; r < m; ++r) {
      getline(cin, line);
      for (size_t c = 0; c < n; ++c) {
        grid[r][c] = tolower(line[c]);
      }
    }

    cin >> k;
    cin.ignore();

    dict_t dict = vector<string>(k);
    for (size_t j = 0; j < k; ++j) {
      getline(cin, dict[j]);
      // search dict[j] in grid
      w2l(dict[j]);
      char start = tolower(dict[j][0]);
      bool keep_search = true;

      for (size_t r = 0; r < m && keep_search; ++r) {
        for (size_t c = 0; c < n; ++c) {
          if (grid[r][c] == start &&
              checkvhd(dict[j], grid, r, c, m, n)) {
            cout << r+1 << " " << c+1 << endl;
            keep_search = false;
            break;
          }
        }
      }
    }

    // separate output line
    if (i != ntest-1) {
      cout << endl;
    }
  }

  return 0;
}
