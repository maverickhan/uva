/*
  UVA online judge Problem P10049: Self-describing Sequence
  Let F(n) be n's longest reachable index,
  then sum_{1, k-1}F(i) < x <= sum_{1, k}F(i)
  then k is the output for fn(x)
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

size_t fn(size_t k)
{
  // base case
  if (k == 1)
    return 1;
  if (k == 2 || k == 3)
    return 2;

  vector<size_t> v = {1, 2, 2};
  size_t i = 3;
  size_t curr = 3;
  size_t sum = 5;
  while (i < k){
    size_t lb = i;
    size_t ub = i + v[curr-1] - 1;
    for (size_t j = lb; j <= ub; ++j) {
      sum += curr;
      if (sum >= k) {
        return j+1;
      }
      v.push_back(curr);
    }
    i = ub + 1;
    curr++;
  }

  // should not reach here
  return k;
}

int main()
{
  size_t k;
  do {
    cin >> k;
    cin.ignore();

    if (k != 0) {
      cout << fn(k) << endl;
    }
  } while (k != 0);

  return 0;
}

