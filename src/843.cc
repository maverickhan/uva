/*
  UVA online judge Problem 843: Crypt Kicker
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

// tbl - decrypted table
// key: char in encrypted text
// val: correponding char in dictionary

bool try_match(const string& w, const string& t,
               unordered_map<char, char>& tbl,
               vector<int>& pos)
{
  if (w.size() != t.size())
    return false;

  for (size_t i = 0; i < w.size(); ++i) {

    // when current char presents in tbl and contradicts with
    // to-be-matched word, matching failed.
    for (auto it = tbl.begin(); it != tbl.end(); ++it) {
      if ((it->first == w[i] && it->second != t[i]) ||
          (it->second == t[i] && it->first != w[i])) {
        // cout << "contradict: " << tbl[w[i]] << endl;
        return false;
      }
    }

    if (tbl.find(w[i]) == tbl.end()) {
      tbl.insert(pair<char, char>(w[i], t[i]));
      pos.push_back(i);
    }
  }

  return true;
}

void undo_update_tbl(const string& w, const string& t,
                     unordered_map<char, char>& tbl,
                     const vector<int>& pos)
{
  if (w.size() != t.size())
    return ;

  for (auto it = pos.begin(); it != pos.end(); ++it) {
    tbl.erase(w[*it]);
  }
}

// @return true -- match succeeds
//         false -- no matching
bool backtrack_match( size_t i,
                      vector<string>& wlist,
                      unordered_map<size_t, vector<string>>& dict,
                      unordered_map<char, char>& tbl
                    )
{
  if (i == wlist.size()) {
    return true;
  }

  string w = wlist[i];
  size_t sz = w.size();
  for (auto it = dict[sz].begin(); it != dict[sz].end(); ++it) {
    vector<int> pos = {};
    bool success = try_match(w, *it, tbl, pos);
    if (success) {
      if (backtrack_match(i+1, wlist, dict, tbl)) {
        return true;
      }
    }
    undo_update_tbl(w, *it, tbl, pos);
  }

  // Tried all words of same length but no match
  return false;
}

int main()
{
  // dictionary: key - word length
  //             val - bucket of words with same len
  unordered_map<size_t, vector<string>> dict;
  size_t dict_size;
  cin >> dict_size;
  cin.ignore();

  // Read in dictionary and organize in word size
  string dict_word;
  for (size_t i = 0; i < dict_size; ++i) {
    getline(cin, dict_word);
    size_t sz = dict_word.size();
    if (dict.find(sz) != dict.end()) {
      dict[sz].push_back(dict_word);
    } else {
      vector<string> v = {dict_word};
      dict.insert(pair<size_t, vector<string>>(sz, v));
    }
  }

  string line;
  // iterate over encrypted text
  while (getline(cin, line)) {
    string word;
    stringstream ss(line);
    vector<string> wlist;
    // iterate word by word
    while (getline(ss, word, ' ')) {
      wlist.push_back(word);
    }

    // tbl summarizes characters already decypted
    unordered_map<char, char> tbl;

    sort(wlist.begin(), wlist.end(),
          [](const string& a, const string& b){
              return a.size() > b.size();
        });

    // Backtracking recursion
    backtrack_match(0, wlist, dict, tbl);

    // Final output
    for (size_t i = 0; i < line.size(); ++i) {
      if (line[i] == ' ') {
        cout << ' ';
      } else {
        if (tbl.find(line[i]) != tbl.end()) {
          cout << tbl[line[i]];
        } else {
          cout << '*';
        }

      }
    }
    cout << endl;
  }

  return 0;
}