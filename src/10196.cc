/*
  UVA online judge Problem p10196: Check the Check
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#define SIZE 8

using namespace std;

using board_t = vector<vector<char>>;
using pos_t   = pair<size_t, size_t>;

// 0 - valid board
// 1 - empty board
bool read_board(board_t& b, pos_t& wk, pos_t& bk)
{
  string line;
  bool is_empty = true;

  for (size_t i = 0; i < SIZE; ++i) {
    getline(cin, line);
    for (size_t j = 0; j < SIZE; ++j) {
      b[i][j] = line[j];
      // only check at the first non '.' char
      if (is_empty && b[i][j] != '.') {
        is_empty = false;
      }
      if (b[i][j] == 'K') {
        wk.first = i;
        wk.second = j;
      }
      if (b[i][j] == 'k') {
        bk.first = i;
        bk.second = j;
      }
    }
  }
  cin.ignore();

  return is_empty;
}

bool horizontal_clear(const board_t& b, size_t i, size_t j, int d)
{
  size_t n = d >= 0 ? d-1 : -1*d-1;
  while (n > 0) {
    if (d < 0) {
      j--; // move left
    } else {
      j++; // move right
    }
    if (b[i][j] != '.') {
      return false;
    }
    n--;
  }

  return true;
}

bool vertical_clear(const board_t& b, size_t i, size_t j, int d)
{
  size_t n = d >= 0 ? d-1 : -1*d-1;
  while (n > 0) {
    if (d < 0) {
      i--; // move up
    } else {
      i++; // move down
    }
    if (b[i][j] != '.') {
      return false;
    }
    n--;
  }

  return true;
}

bool diagonal_clear(const board_t& b, size_t i, size_t j, int d1, int d2)
{
  size_t n = d1>=0 ? d1-1 : -1*d1-1;
  while (n > 0) {
    if (d1 < 0) {
      i--; // move up
    } else {
      i++; //move down
    }

    if (d2 < 0) {
      j--; // move left
    } else {
      j++; // move right
    }
    if (b[i][j] != '.') {
      return false;
    }

    n--;
  }

  return true;
}

// white -- upper case & bottom
// black -- lower case & top
// @return: 1  - white king is in check
//          0  - no king is in check
//          -1 - black king is in check
int check(const board_t& board, const pos_t& wk, const pos_t& bk)
{
  bool white_check = false;
  bool black_check = false;
  size_t x, y;
  int hd, vd;
  for (size_t i = 0; i < SIZE; ++i) {
    for (size_t j = 0; j < SIZE; ++j) {
      switch(board[i][j]) {
        case 'p':
          x = wk.first;
          y = wk.second;
          if ((i+1) == x &&
              ((j-1) == y || (j+1) == y)) {
            white_check = true;
          }
          break;
        case 'P':
          x = bk.first;
          y = bk.second;
          if ((i-1) == x &&
              ((j-1) == y || (j+1) == y)) {
            black_check = true;
          }
          break;
        case 'n':
          x = wk.first;
          y = wk.second;
          if (
              ((i+1) == x && ((j-2) == y || (j+2) == y)) ||
              ((i-1) == x && ((j-2) == y || (j+2) == y)) ||
              ((i+2) == x && ((j-1) == y || (j+1) == y)) ||
              ((i-2) == x && ((j-1) == y || (j+1) == y))
             ) {
            white_check = true;
          }
          break;
        case 'N':
          x = bk.first;
          y = bk.second;
          if (
              ((i+1) == x && ((j-2) == y || (j+2) == y)) ||
              ((i-1) == x && ((j-2) == y || (j+2) == y)) ||
              ((i+2) == x && ((j-1) == y || (j+1) == y)) ||
              ((i-2) == x && ((j-1) == y || (j+1) == y))
             ) {
            black_check = true;
          }
          break;
        case 'b':
          x = wk.first;
          y = wk.second;
          hd = x - i;
          vd = y - j;
          if (abs(hd) == abs(vd) &&
              diagonal_clear(board, i, j, hd, vd)) {
            white_check = true;
          }
          break;
        case 'B':
          x = bk.first;
          y = bk.second;
          hd = x - i;
          vd = y - j;
          if (abs(hd) == abs(vd) &&
              diagonal_clear(board, i, j, hd, vd)) {
            black_check = true;
          }
          break;
        case 'r':
          x = wk.first;
          y = wk.second;
          hd = x - i;
          vd = y - j;
          if (
              ((x == i) && horizontal_clear(board, i, j, vd)) ||
              ((y == j) && vertical_clear(board, i, j, hd))
             ) {
            white_check = true;
          }
          break;
        case 'R':
          x = bk.first;
          y = bk.second;
          hd = x - i;
          vd = y - j;
          if (
              ((x == i) && horizontal_clear(board, i, j, vd)) ||
              ((y == j) && vertical_clear(board, i, j, hd))
             ) {
            black_check = true;
          }
          break;
        case 'q':
          x = wk.first;
          y = wk.second;
          hd = x - i;
          vd = y - j;
          if (
              ((abs(hd) == abs(vd)) && diagonal_clear(board, i, j, hd, vd)) ||
              ((x == i) && horizontal_clear(board, i, j, vd)) ||
              ((y == j) && vertical_clear(board, i, j, hd))
             ) {
            white_check = true;
          }
          break;
        case 'Q':
          x = bk.first;
          y = bk.second;
          hd = x - i;
          vd = y - j;
          if (
              ((abs(hd) == abs(vd)) && diagonal_clear(board, i, j, hd, vd)) ||
              ((x == i) && horizontal_clear(board, i, j, vd)) ||
              ((y == j) && vertical_clear(board, i, j, hd))
             ) {
            black_check = true;
          }
          break;
        case 'k':
          x = wk.first;
          y = wk.second;
          hd = x - i;
          vd = y - j;
          if (abs(hd) <= 1 && abs(vd) <= 1) {
            white_check = true;
          }
          break;
        case 'K':
          x = bk.first;
          y = bk.second;
          hd = x - i;
          vd = y - j;
          if (abs(hd) <= 1 && abs(vd) <= 1) {
            black_check = true;
          }
          break;
        default:
          break;
      }

      if (white_check) {
        return 1;
      }

      if (black_check) {
        return -1;
      }
    }
  }

  return 0;
}

int main()
{
  board_t b(SIZE, vector<char>(SIZE));
  size_t cnt = 1;
  // position of the king
  pos_t wk, bk;
  while (!read_board(b, wk, bk)) {
    // Check board
    int status = check(b, wk, bk);

    cout << "Game #" << cnt << ": ";

    if (status == 1) {
      cout << "white king is in check." << endl;
    } else if (status == -1) {
      cout << "black king is in check." << endl;
    } else {
      cout << "no king is in check." << endl;
    }

    cnt++;
  }
}