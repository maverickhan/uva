/*
  UVA online judge Problem P10050: Hartals
  vairabile length bitset
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

class hartal {
public:
  hartal(){}

  hartal(size_t m) : _sz(m) {
    _ht.reserve(m);
    for (size_t i = 0; i < m; ++i) {
      _ht[i] = false;
    }
  }

  void init(size_t m, size_t n) {
    _sz = m;
    _ht.reserve(m);

    for (size_t i = 0; i < m; ++i) {
      if ((i+1) % n == 0) {
        _ht[i] = true;
      } else {
        _ht[i] = false;
      }
    }
  }

  void set(const size_t pos) {
    _ht[pos] = true;
  }

  bool test(const size_t pos) const {
    return _ht[pos];
  }

  inline size_t size() const {
    return _sz;
  }

  hartal& operator|=(const hartal& rhs) {
    if (_sz != rhs.size()) {
      cout << "size mismatch!" << endl;
      return *this;
    }

    for (size_t i = 0; i < _sz; ++i) {
      _ht[i] = _ht[i] | rhs._ht[i];
    }

    return *this;
  }

  void print() const {
    for (size_t i = 0; i < _sz; ++i) {
      if (_ht[i] == true) {
        cout << "1";
      } else {
        cout << "0";
      }
    }
    cout << endl;
  }

private:
  vector<bool> _ht;
  size_t _sz;
};

hartal operator|(hartal lhs, const hartal& rhs)
{
  lhs |= rhs;
  return lhs;
}

int main()
{
  size_t num_test;
  cin >> num_test;
  cin.ignore();

  for (size_t k = 0; k < num_test; ++k) {
    size_t N;
    cin >> N;
    cin.ignore();

    size_t m;
    cin >> m;
    cin.ignore();

    vector<hartal> h(m);
    hartal identity(N);
    for (size_t i = 0; i < m; ++i) {
      size_t sz;
      cin >> sz;
      cin.ignore();
      h[i].init(N, sz);
      // h[i].print();
      identity |= h[i];
    }

    size_t strike = 0;
    for (size_t i = 0; i < N; ++i) {
      if (((i+1) % 7 != 0) && ((i+2) % 7 != 0)) {
        if (identity.test(i) == true) {
          strike++;
        }
      }
    }

    cout << strike << endl;

  }

  return 0;
}
