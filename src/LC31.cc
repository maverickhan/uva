/*
  LeetCode 31: Next Permutation

   - From right to left, find the first number which violates increasing, save its index
   - Swap partition number with the first of the increasing sequence, which should be the last one.
   - Reverse all digits to the right of partition index
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

// reverse elements in v from begin to end
void reverse_sequence(vector<int>& v, int begin, int end)
{
  int mid = begin + (end - begin) / 2;
  if ((end - begin + 1) % 2 == 0){
    mid++;
  }

  int temp;
  for (int i = begin, j = end; i < mid; ++i, --j) {
    temp = v[i];
    v[i] = v[j];
    v[j] = temp;
  }
}

int main()
{
  size_t n;
  cin >> n;
  cin.ignore();

  for (size_t k = 0; k < n; ++k) {
    string line;
    getline(cin, line);
    stringstream ss(line);
    string num;
    vector<int> v;
    while(getline(ss, num, ' ')) {
      v.push_back(stoi(num));
    }

    int n = v.size();
    int i;
    for (i = n-2; i >= 0; --i) {
      if(v[i] < v[i+1]) {
        int temp = v[i];
        v[i] = v[n-1];
        v[n-1] = temp;
        break;
      }
    }
    if (i >= 0) {
      reverse_sequence(v, i+1, n-1);
    } else { // we need to reverse the whole seq
      reverse_sequence(v, 0, n-1);
    }

    for (const auto& i : v) {
      cout << i << ' ';
    }
    cout << endl;
  }

  return 0;
}

