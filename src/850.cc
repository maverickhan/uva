/*
  UVA online judge Problem P850: Crypt Kicker II
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

vector<string> text = {"the", "quick", "brown",
                       "fox", "jumps", "over",
                       "the", "lazy", "dog"};

bool size_match(vector<string>& orig, vector<string>& encrypt)
{
  if (orig.size() != encrypt.size()) {
    return false;
  }

  vector<string>::iterator it1, it2;
  for (it1= orig.begin(), it2 = encrypt.begin();
       it1 != orig.end();
       it1++, it2++) {
    if (it1->size() != it2->size()) {
      return false;
    }
  }

  return true;
}

bool try_match(vector<string>& orig, vector<string>& encrypt, unordered_map<char, char>& d)
{
  vector<string>::iterator it1, it2;
  for (it1 = orig.begin(), it2 = encrypt.begin();
       it1 != orig.end();
       it1++, it2++)
  {
    size_t sz = it1->size();
    for (size_t i = 0; i < sz; ++i) {
      char c1 = (*it1)[i];
      char c2 = (*it2)[i];
      if (d.find(c2) == d.end()) {
        d.insert(pair<char, char>(c2, c1));
      } else if (d[c2] != c1) {
        return false;
      } else {
        continue;
      }
    }
  }

  return true;
}

int main()
{
  size_t n;
  cin >> n;
  cin.ignore();
  // ignore the first empty line
  string line;
  getline(cin, line);

  for (size_t i = 0; i < n; ++i) {
    bool has_match = false;
    vector<string> line_list;
    unordered_map<char, char> dict;
    while(getline(cin, line)) {
      // go to the next case
      if (line.empty()) {
        break;
      }

      vector<string> wlist;
      stringstream ss(line);
      string word;
      while (getline(ss, word, ' ')) {
        wlist.push_back(word);
      }

      if (size_match(text, wlist)) {
        has_match = try_match(text, wlist, dict);
      }

      line_list.push_back(line);
    }

    if (has_match) {
      // output line by line
      for (size_t j = 0; j < line_list.size(); ++j) {
        for (size_t k = 0; k < line_list[j].size(); ++k) {
          char c = line_list[j][k];
          if (c == ' ') cout << ' ';
          else if (dict.find(c) != dict.end()) {
            cout << dict[c];
          }
        }
        cout << endl;
      }
    } else {
      cout << "No solution." << endl;
    }

    line_list.clear();
    dict.clear();

    if (i != n-1) {
      cout << endl;
    }
  }

  return 0;
}