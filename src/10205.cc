/*
  UVA online judge Problem P10205: Stack 'Em Up
  Reverse indexing: old index -> new index
  sort based on new index, referenced by old index
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#define N 52

using namespace std;

string suites[4] = {"Clubs",
                    "Diamonds",
                    "Hearts",
                    "Spades"};
string values[13] = {"2", "3", "4", "5",
                     "6", "7", "8", "9",
                     "10", "Jack", "Queen",
                     "King", "Ace"};

struct card {
  string suite;
  string val;
  size_t pos;

  card() : suite("Clubs"), val("2"), pos(0) {}
  card(const card& c) {
    suite = c.suite;
    val = c.val;
    pos = c.pos;
  }
  card(const card&& c) {
    suite = move(c.suite);
    val = move(c.val);
    pos = move(c.pos);
  }
  card& operator=(const card& c) {
    if (this != &c) {
      suite = c.suite;
      val = c.val;
      pos = c.pos;
    }
    return *this;
  }
  card& operator=(const card&& c) {
    suite = move(c.suite);
    val = move(c.val);
    pos = move(c.pos);
    return *this;
  }
};

void initialize_deck(vector<card>& d)
{
  for (size_t i = 0; i < 4; ++i) {
    for (size_t j = 0; j < 13; ++j) {
      d[j+i*13].suite = suites[i];
      d[j+i*13].val   = values[j];
      d[j+i*13].pos   = j+i*13;
    }
  }
}

void print_deck(vector<card>& d)
{
  for (const auto& i : d) {
    cout << i.val << " of " << i.suite << endl;
  }
}

// restore deck pos to [1, 52]
void reinialize_deck(vector<card>& d)
{
  size_t index = 0;
  for (auto& i : d) {
    i.pos = index;
    index++;
  }
}

void swap_card(vector<card>& d, size_t i, size_t j)
{
  card temp;
  temp = move(d[i]);
  d[i] = move(d[j]);
  d[j] = move(temp);
}

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();
  cin.ignore();
  for (size_t i = 0; i < ntest; ++i) {
    string line;

    size_t nshuffle;
    cin >> nshuffle;
    cin.ignore();
    vector<vector<size_t>> v(nshuffle, vector<size_t>(N));

    vector<card> deck(N);
    initialize_deck(deck);

    // store shuffles
    for (size_t j = 0; j < nshuffle; ++j) {
      getline(cin, line);
      stringstream ss(line);
      string pos;
      size_t k = 0;
      // reverse indexing
      while(getline(ss, pos, ' ')) {
        size_t p = stoi(pos);
        v[j][p-1] = k++;
      }
    }

    // read in observed shuffles
    while (getline(cin, line)) {
      // encounter the empty line
      if (line == "") {
        break;
      }
      size_t k = stoi(line);
      sort(deck.begin(), deck.end(),
           // compare function
           // c1 is less than c2 when its indexed
           [&v, k](const card& c1, const card& c2){
              return v[k-1][c1.pos] < v[k-1][c2.pos];
           });
      reinialize_deck(deck);
    }

    print_deck(deck);
    // separate test cases
    cout << endl;
  }

  return 0;
}
