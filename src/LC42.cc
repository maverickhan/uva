/*
  Leet Code 42: Trapping rain water
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main()
{
  size_t n;
  cin >> n;
  cin.ignore();

  for (size_t i = 0; i < n; ++i) {
    string line;
    getline(cin, line);
    stringstream ss(line);

    string num;
    vector<int> w;
    while (getline(ss, num, ' ')) {
      w.push_back(stoi(num));
    }

    // discard initial increasing sequence as it cannot hold any water
    size_t start_pos = 0;
    for (size_t i = 1; i < w.size(); ++i) {
      if (w[i] >= w[i-1]) start_pos = i;
      else break;
    }

    int left_top = w[start_pos], right_top = INT_MAX;
    int blocks = w[start_pos];
    int length = 0;
    int volume = 0;
    for (size_t i = start_pos+1; i < w.size(); ++i) {
      // going down hill, meaning the last 'v'-shape is complete.
      if (w[i] <= w[i-1] && right_top != INT_MAX) {
        volume += min(left_top, right_top) * length - blocks;
        left_top = w[i-1];
        right_top = INT_MAX;
        length = 1;
        blocks = w[i];
      }

      // going down hill, still in forming new 'v'-shape.
      if (w[i] <= w[i-1] && right_top == INT_MAX) {
        length++;
        blocks += w[i];
      }

      // climbing up hill
      if (w[i] > w[i-1]) {
        right_top = w[i];
        length++;
        if (right_top <= left_top) {
          blocks += w[i];
        }
      }

      cout << w[i] << ": " << length << " " << blocks << " " << volume << endl;
    }

    volume += min(left_top, right_top) * length - blocks;

    cout << volume << endl;
  }

  return 0;
}

