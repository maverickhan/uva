/*
  LeetCode 04: Meidan of two sorted arrays
  O(log(m+n))
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int findKth(const vector<int>& a, int m,
            const vector<int>& b, int n,
            int k)
{
  if (m >= a.size()) {
    return b[n+k-1];
  }
  if (n >= b.size()) {
    return a[m+k-1];
  }
  if (k == 1) {
    return min(a[m], b[n]);
  }

  int akey = m+k/2-1 < a.size() ?
             a[m+k/2-1] : INT_MAX;
  int bkey = n+k/2-1 < b.size() ?
             b[n+k/2-1] : INT_MAX;
  if (akey < bkey) {
    return findKth(a, m+k/2, b, n, k-k/2);
  } else {
    return findKth(a, m, b, n+k/2, k-k/2);
  }
}

float findMedian(const vector<int>& a, const vector<int>& b)
{
  int len = a.size() + b.size();
  if (len % 2 == 1) {
    return findKth(a, 0, b, 0, len / 2 + 1);
  } else {
    return (findKth(a, 0, b, 0, len / 2) + findKth(a, 0, b, 0, len / 2 + 1)) / 2.0;
  }
}

int main()
{
  size_t num_tests;
  cin >> num_tests;
  cin.ignore();

  for (size_t k = 0; k < num_tests; ++k) {
    vector<int> a;
    vector<int> b;

    string line;
    getline(cin, line);
    stringstream ss1(line);
    string num;
    while(getline(ss1, num, ' ')) {
      a.push_back(stoi(num));
    }
    getline(cin, line);
    stringstream ss2(line);
    while(getline(ss2, num, ' ')) {
      b.push_back(stoi(num));
    }

    float median = findMedian(a, b);
    cout << "test case " << k << ": " << median << endl;
  }

  return 0;
}

