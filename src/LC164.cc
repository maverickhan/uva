/*
  LeetCode 164: Maximum Gap
  Use bucket sort to divide n-2 elements to n-1 buckets (exlude min and max)
  One bucket must be empty
  That means, the maximum gap must be at least of bucket length
  Also means, maximum gap cannot happen within buckets.
  Find maximum distance by calculating the min and max for each bucket.
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int findMaxGap(const vector<int>& v)
{
  size_t n = v.size();
  if (n < 2) {
    return 0;
  }

  // find min and max in one pass
  int min = v[0], max = v[0];
  for (size_t i = 1; i < n; ++i) {
    if (v[i] < min) min = v[i];
    if (v[i] > max) max = v[i];
  }

  // calculate bucket length
  int bucket_len = (max - min) / n + 1;

  // construct buckets
  vector<vector<int>> buckets((max - min) / bucket_len + 1);
  for (int x : v) {
    int i = (x - min) / bucket_len;
    if (buckets[i].empty()) {
      buckets[i].reserve(2);
      buckets[i].push_back(x);
      buckets[i].push_back(x);
    } else {
      if (x < buckets[i][0]) buckets[i][0] = x;
      if (x > buckets[i][1]) buckets[i][1] = x;
    }
  }

  int gap = 0;
  int prev = 0;
  for (size_t i = 0; i < buckets.size(); ++i) {
    if (buckets[i].empty()) continue;
    int diff = buckets[i][0] - buckets[prev][1];
    gap = diff > gap ? diff : gap;
    prev = i;
  }

  return gap;
}

int main()
{
  size_t num_test;
  cin >> num_test;
  cin.ignore();

  for (size_t k = 0; k < num_test; ++k) {
    string line;
    getline(cin, line);
    stringstream ss(line);
    string num;
    vector<int> v;
    while(getline(ss, num, ' ')) {
      v.push_back(stoi(num));
    }

    int max_gap = findMaxGap(v);
    cout << "max gap: " << max_gap << endl;
  }

  return 0;
}

