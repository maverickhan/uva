/*
  LeetCode 97: Interleaving Strings
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

bool is_interleaved_helper(int i1, const string& s1,
													 int i2, const string& s2,
													 int i3, const string& s3)
{
	if (i3 == s3.size()) {
		return true;
	} else {
		bool f1 = false;
		if (s1[i1] == s3[i3]) {
			f1 = is_interleaved_helper(i1+1, s1, i2, s2, i3+1, s3);
		}
		if (f1) {
			return true;
		} else if (s2[i2] == s3[i3]) {
			return is_interleaved_helper(i1, s1, i2+1, s2, i3+1, s3);
		}
	}

	return false;
}

bool is_interleaved(const string& s1, const string& s2, const string& s3)
{
	if (s1.size() + s2.size() != s3.size())
		return false;

	bool f1 = false;

	if (s1[0] == s3[0]) {
		f1 = is_interleaved_helper(1, s1, 0, s2, 1, s3);
	}

	if (f1) {
		return true;
	} else if (s2[0] == s3[0]) {
		return is_interleaved_helper(0, s1, 1, s2, 1, s3);
	}

	return false;
}

int main()
{
	string s1, s2;
	getline(cin, s1);
	getline(cin, s2);

	size_t n;
	cin >> n;
	cin.ignore();
	for (size_t i = 0; i < n; ++i) {
		string s3;
		getline(cin, s3);
		bool interleave = is_interleaved(s1, s2, s3);

		string output = interleave ? "true" : "false";
		cout << output << endl;
	}

  return 0;
}

