/*
  UVA online judge Problem p706: LC Display
 */

#include <algorithm>
#include <climits>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

void print_horizontal(size_t col, bool flag)
{
  for (size_t i = 0; i < col; ++i) {
    if (i > 0 && i < col-1 && flag) {
      cout << '-';
    } else {
      cout << ' ';
    }
  }
}

void print_vertical(size_t col, size_t pattern)
{
  for (size_t i = 0; i < col; ++i) {
    if ((i == 0 && (pattern == 0 || pattern == 2)) ||
        (i == col-1 && (pattern == 1 || pattern == 2))
       ) {
      cout << '|';
    } else {
      cout << ' ';
    }
  }
}

int main()
{
  size_t s;
  string n;
  cin >> s >> n;
  cin.ignore();

  if (s == 0 && n == "0") {
    return 0;
  }

  while (s > 0 && n != "0") {

    size_t c = s + 2;
    size_t r = 2 * s + 3;

    for (size_t i = 0; i < r; ++i) {
      for (size_t j = 0; j < n.size(); ++j) {
        char d = n[j];
        if (i == 0) {
          if (d == '0' || d == '2' || d == '3' ||
              d == '5' || d == '6' || d == '7' ||
              d == '8' || d == '9'){
            print_horizontal(c, true);
          } else {
            print_horizontal(c, false);
          }
        } else if (i == s + 1) {
          if (d == '2' || d == '3' || d == '4' ||
              d == '5' || d == '6' || d == '8' ||
              d == '9'){
            print_horizontal(c, true);
          } else {
            print_horizontal(c, false);
          }
        } else if (i == 2 * s + 2) {
          if (d == '0' || d == '2' || d == '3' ||
              d == '5' || d == '6' || d == '8' ||
              d == '9') {
            print_horizontal(c, true);
          } else {
            print_horizontal(c, false);
          }
        } else if (i > 0 && i < s+1) {
          if (d == '5' || d == '6') {
            print_vertical(c, 0);
          } else if (d == '1' || d == '2' ||
                     d == '3' || d == '7') {
            print_vertical(c, 1);
          } else {
            print_vertical(c, 2);
          }
        } else {
          if (d == '2') {
            print_vertical(c, 0);
          } else if (d == '1' || d == '3' ||
                     d == '4' || d == '5' ||
                     d == '7' || d == '9') {
            print_vertical(c, 1);
          } else {
            print_vertical(c, 2);
          }
        }
        // space between digits
        cout << ' ';
      }
      cout << endl;
    }

    cin >> s >> n;
    cin.ignore();
    cout << endl;
  }

  return 0;
}