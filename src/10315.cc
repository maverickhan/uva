/*
  UVA online judge Problem P10315: Poker Hand
  Reduce hand to one single value
  compare hand or compare card: sort them as needed
  corner case: flush needs to resort hand, otherwise compare return the wrong
               result due to compare the potential pair or three/four kind first.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

enum category : size_t {
  one_pair       = 15,
  two_pair       = 16,
  three_a_kind   = 17,
  straight       = 18,
  flush          = 19,
  full_house     = 20,
  four_a_kind    = 21,
  straight_flush = 22
};

struct card {
  char suite;
  size_t value;

  card() : suite('H'), value(0) {}
};

struct hand {
  vector<card> c;
  size_t r;

  hand() {
    c = vector<card>(5);
    r = 0;
  }
};

size_t get_val(char c) {
  if (c == 'T') {
    return 10;
  } else if (c == 'J') {
    return 11;
  } else if (c == 'Q') {
    return 12;
  } else if (c == 'K') {
    return 13;
  } else if (c == 'A') {
    return 14;
  } else {
    return c - '0';
  }
}

size_t compute_rank(const hand& h, const unordered_map<size_t, size_t>& m, 
                    bool is_straight, bool same_suite) {

  // see if it's a straight flush
  if (is_straight && same_suite) {
    return category::straight_flush;
  }

  bool has_two = false, has_three = false;
  size_t num_pair = 0;
  for (const auto& i : m) {
    if (i.second == 2) {
      has_two = true;
      num_pair++;
    }
    if (i.second == 3) has_three = true;
    if (i.second == 4) return category::four_a_kind;
  }

  if (has_three && has_two) {
    return category::full_house;
  }

  if (same_suite == true) {
    return category::flush;
  }

  if (is_straight == true) {
    return category::straight;
  }

  if (has_three == true) {
    return category::three_a_kind;
  }

  if (num_pair == 2) {
    return category::two_pair;
  }

  if (num_pair == 1) {
    return category::one_pair;
  }

  return h.c[4].value;
}

int compare_hand(const hand& a, const hand& b)
{
  for (int i = 4; i >= 0; --i) {
    if (a.c[i].value > b.c[i].value) return 1;
    else if (a.c[i].value < b.c[i].value) return -1;
    else continue;
  }
  return 0;
}

int compare_card(const card& a, const card& b)
{
  if (a.value > b.value) return 1;
  else if (a.value < b.value) return -1;
  else return 0;
}

int compare(hand& black, hand& white) {
  if (black.r > white.r) return 1;
  else if (black.r < white.r) return -1;
  else {
    size_t r = black.r;
    if (r <= 17 || r == 21) {
      return compare_hand(black, white);
    } else if (r == 18 || r == 20 || r == 22) {
      return compare_card(black.c[4], white.c[4]);
    } else if (r == 19) {
      // if same suite (flush), rank according to high card
      sort(black.c.begin(), black.c.end(), 
          [](const card& c1, const card& c2) {
            return c1.value < c2.value;
          });
      sort(white.c.begin(), white.c.end(), 
          [](const card& c1, const card& c2) {
            return c1.value < c2.value;
          });
      return compare_hand(black, white);
    } else {
      cout << "Unexpected rank number!\n";
      return 0;
    }
  }
}

int main()
{
  string line;
  while (getline(cin, line)) {
    hand h[2];
    for (size_t i = 0; i < 10; ++i) {
      char c = line[3*i];
      h[i/5].c[i%5].value = get_val(c);
      h[i/5].c[i%5].suite = line[3*i+1];
    }

    for (size_t i = 0; i < 2; ++i) {
      // count cards for each hand
      unordered_map<size_t, size_t> m;
      size_t single_count = 0;

      // used to check same suite
      char s = h[i].c[0].suite;
      bool same_suite = true;

      for (size_t j = 0; j < 5; ++j) {
        size_t val = h[i].c[j].value;
        if (m.find(val) != m.end()) {
          m[val]++;
        } else {
          m.insert(pair<size_t, size_t>(val, 1));
          single_count++;
        }

        // check same suite
        if (j > 0 && same_suite) {
          if (h[i].c[j].suite != s) {
            same_suite = false;
          }
        }
      }

      // sort card, first sort according to occurance, then value
      sort(h[i].c.begin(), h[i].c.end(),
           [&m](const card& c1, const card& c2){
            if (m[c1.value] < m[c2.value]) return true;
            else if (m[c1.value] > m[c2.value]) return false;
            else return c1.value < c2.value;
        });

      // check if hand has a straight
      bool is_straight;
      if (single_count == 5) {
        is_straight = true;
        size_t old_val = h[i].c[0].value;
        for (size_t j = 1; j < 5; ++j) {
          if (h[i].c[j].value != old_val + 1) {
            is_straight = false;
            break;
          } else {
            old_val = h[i].c[j].value;
          }
        }
      } else {
        is_straight = false;
      }

      // compute rank for each hand
      h[i].r = compute_rank(h[i], m, is_straight, same_suite);
      // debug
      // cout << i << ": " << h[i].r << endl;
      // string s1 = is_straight ? "true" : "false";
      // string s2 = same_suite ? "true" : "false";
      // cout << "is_straight? " << s1 << endl;
      // cout << "same_suite? " << s2 << endl;
      // for (const auto& i: h[i].c) {
      //   cout << i.value << " ";
      // }
      // cout << endl;
    }

    int ret = compare(h[0], h[1]);

    if (ret == 1) {
      cout << "Black wins." << endl;
    } else if (ret == -1) {
      cout << "White wins." << endl;
    } else {
      cout << "Tie." << endl;
    }
  }

  return 0;
}
