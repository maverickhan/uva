/*
  UVA online judge Problem 10138: CDVII
  one dollar per trip
  two dollars account charge
  each km at a predetermined rate

  Note that exit mile might be less than enter mile, so add abs to make sure
  the value is always positive
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define H 24

using namespace std;

struct record {
	int t[4];
	string action;
	int km;

	record(int mm, int dd, 
		     int hh, int mi, 
		     string a, int km) : action(a), km(km) {
		t[0] = mm;
		t[1] = dd;
		t[2] = hh;
		t[3] = mi;
	}
};

int main()
{
	size_t ntest;
	cin >> ntest;
	cin.ignore();

	string line;
	getline(cin, line);
	for (size_t i = 0; i < ntest; ++i) {
		vector<float> rate(H);
		getline(cin, line);
		stringstream ss(line);
		string word;
		for (size_t i = 0; i < H; ++i) {
			getline(ss, word, ' ');
			rate[i] = stoi(word) * 0.01;
		}

		map<string, vector<record>> history;
		while (getline(cin, line)) {
			if (line == "") {
				break;
			}

			istringstream iss(line);
			string car_num;
			string time;
			string action;
			string km;

			iss >> car_num >> time >> action >> km;
			int tc[4];
			stringstream ss(time);
			string x;
			for (size_t i = 0; i < 4; ++i) {
				getline(ss, x, ':');
				tc[i] = stoi(x);
			}

			record r(tc[0], tc[1], tc[2], tc[3], action, stoi(km));
			if (history.find(car_num) != history.end()) {
				history[car_num].push_back(r);
			} else {
				history.insert(pair<string, vector<record>>(car_num, vector<record>(1, r)));
			}
		}

		for (auto it = history.begin(); it != history.end(); ++it) {
			float cost = 0;
			auto& v = it->second;
			// sort record based on timestamp
			sort(v.begin(), v.end(), [](const record& r1, const record& r2){
				if (r1.t[1] < r2.t[1]) {
					return true;
				} else if (r1.t[1] > r2.t[1]) {
					return false;
				} else { // tie at day
					if (r1.t[2] < r2.t[2]) {
						return true;
					} else if (r1.t[2] > r2.t[2]) {
						return false;
					} else { // tie at hour
						if (r1.t[3] < r2.t[3]) {
							return true;
						} else {
							return false;
						}
					}
				}
			});

			size_t i = 0;
			while (i < v.size()-1) {
				// skip unpaired enter or exit
				if (v[i].action == "exit" || v[i+1].action == "enter") {
					i++;
					continue;
				} else {
					float curr_rate = rate[v[i].t[2]];
					int dist = abs(v[i+1].km - v[i].km);
					cost += curr_rate * dist;
					cost += 1.00;
					i += 2;
				}
			}

			if (cost > 0.0) {
				cost += 2.00;
				cout << it->first << " $" << setprecision(2) << fixed<< cost << endl;
			}
		}

		if (i != ntest-1) {
			cout << endl;
		}
	}

  return 0;
}
