/*
  UVA online judge Problem p10033: interpreter
  *** WA ***
  - uint8_t -> char, output by cout as char.
 */

#include <algorithm>
#include <climits>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

#define REG_SIZE 10
#define RAM_SIZE 1000

size_t reg[REG_SIZE];
string ram[RAM_SIZE];

int to_int(const string& s)
{
  return (s[0] - '0') * 100 + (s[1] - '0') * 10 + (s[2] - '0');
}

string to_str(int x)
{
  string s(3, '0');
  int div = 100, i = 0;
  while (div >= 1) {
    s[i++] = '0' + (x / div);
    x %= div;
    div /= 10;
  }

  return s;
}

void init_ram()
{
  for (size_t i = 0; i < RAM_SIZE; ++i) {
    ram[i] = "000";
  }
}

void clear_register()
{
  for (size_t i = 0; i < REG_SIZE; ++i) {
    reg[i] = 0;
  }
}

void read_ram()
{
  string line;
  size_t i = 0;
  while(getline(cin, line)) {
    ram[i++] = line;
    if (line.empty()) {
      break;
    }
  }
}

// 100 means halt
// 2dn means set register d to n (between 0 and 9)
// 3dn means add n to register d
// 4dn means multiply register d by n
// 5ds means set register d to the value of register s
// 6ds means add the value of register s to register d
// 7ds means multiply register d by the value of register s
// 8da means set register d to the value in RAM whose address is in register a
// 9sa means set the value in RAM whose address is in register a to the value of register s
// 0ds means goto the location in register d unless register s contains 0

void execute()
{
  bool exit = false;
  size_t counter = 0;
  size_t src, dest;
  size_t operand, addr;
  size_t i = 0;
  while (!exit) {
    char op = ram[i][0];
    switch(op) {
      case '1':
        exit = true;
        counter++;
        break;
      case '2':
        dest = ram[i][1] - '0';
        reg[dest] = ram[i][2] - '0';
        counter++;
        break;
      case '3':
        dest = ram[i][1] - '0';
        operand = ram[i][2] - '0';
        reg[dest] = (reg[dest] + operand) % RAM_SIZE;
        counter++;
        break;
      case '4':
        dest = ram[i][1] - '0';
        operand = ram[i][2] - '0';
        reg[dest] = (reg[dest] * operand) % RAM_SIZE;
        counter++;
        break;
      case '5':
        dest = ram[i][1] - '0';
        src = ram[i][2] - '0';
        reg[dest] = reg[src];
        counter++;
        break;
      case '6':
        dest = ram[i][1] - '0';
        src = ram[i][2] - '0';
        reg[dest] = (reg[dest] + reg[src]) % RAM_SIZE;
        counter++;
        break;
      case '7':
        dest = ram[i][1] - '0';
        src = ram[i][2] - '0';
        reg[dest] = (reg[dest] * reg[src]) % RAM_SIZE;
        counter++;
        break;
      case '8':
        dest = ram[i][1] - '0';
        addr = ram[i][2] - '0';
        src = reg[addr];
        reg[dest] = to_int(ram[src]);
        counter++;
        break;
      case '9':
        src = ram[i][1] - '0';
        addr = ram[i][2] - '0';
        dest = reg[addr];
        ram[dest] = to_str(reg[src]);
        counter++;
        break;
      case '0':
        dest = ram[i][1] - '0';
        src = ram[i][2] - '0';
        if (reg[src] != 0) {
          i = reg[dest];
          counter++;
          continue;
        } else {
          counter++;
          break;
        }
      default:
        break;
    }
    i++;
  }

  cout << counter << endl;
}

int main()
{
  size_t num_test = 0;
  cin >> num_test;
  cin.ignore();
  cin.ignore();

  while (num_test > 0) {
    init_ram();
    clear_register();
    read_ram();
    execute();

    if (num_test > 1) {
      cout << endl;
    }
    num_test--;
  }

  return 0;
}