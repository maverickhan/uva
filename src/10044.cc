/*
  UVA online judge Problem P10044: Erdos Numbers
 */

#include <algorithm>
#include <climits>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <locale>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#define MAX_NAME_LEN 64
#define INF          std::numeric_limits<size_t>::max()

using namespace std;

struct node_state {
  bool _visited;
  size_t _score;

  node_state() : _visited(false), _score(INF) {}
};

using edge_t = unordered_map<string, vector<string>>;
using node_t  = unordered_map<string, node_state>;

inline string& ltrim(string& s, const char* t = " \t\n\r\f\v")
{
  s.erase(0, s.find_first_not_of(t));
  return s;
}

inline string& rtrim(string& s, const char* t = " \t\n\r\f\v")
{
  s.erase(s.find_last_not_of(t) + 1);
  return s;
}

inline string& trim(string&& s, const char* t = " \t\n\r\f\v")
{
  return ltrim(rtrim(s, t), t);
}

void bfs(const string& s, edge_t& e, node_t& v)
{
  if (!e[s].empty()) {
    for (auto it = e[s].begin(); it != e[s].end(); it++) {
      if (!v[*it]._visited) {
        v[*it]._score = v[s]._score + 1;
        v[*it]._visited = true;
        bfs(*it, e, v);
      }
    }
  }
}

int main()
{
  // processing input
  size_t scenario_id = 1;
  size_t num_scenario;
  cin >> num_scenario;
  cin.ignore();
  while (scenario_id <= num_scenario) {
    size_t p, n;
    cin >> p >> n;
    cin.ignore();

    string paper;
    vector<vector<string>> cliques;
    for (size_t i = 0; i < p; ++i) {
      getline(cin, paper);
      stringstream ss1(paper);
      string author_list;
      getline(ss1, author_list, ':');

      char author[MAX_NAME_LEN];
      vector<string> clique;
      size_t comma_cnt = 0;
      size_t j = 0;
      author_list.append(1u, ',');
      // The logic below handles author name extraction
      // It assumes each author has first and last name
      // in paper database. However, this might not be true
      // in test cases, hence lead to incorrect parsing results.
      for (size_t i = 0; i < author_list.size(); ++i) {
        if (author_list[i] == ',') {
          comma_cnt++;
          if (comma_cnt % 2 == 0) {
            author[j] = '\0';
            string author_name = trim(string(author));
            clique.push_back(author_name);
            // ignore the next white space
            i++;
            j = 0;
            continue;
          }
        }
        author[j++] = author_list[i];
      }

      cliques.push_back(clique);
    }

    string name;
    vector<string> node(n);
    edge_t e;
    node_t v;

    // construct graph
    for (auto it = cliques.begin(); it != cliques.end(); ++it) {
      size_t sz = it->size();
      for (size_t i = 0; i < sz; ++i) {
        for (size_t j = 0; j < sz; ++j) {
          if (j != i) {
            e[(*it)[i]].push_back((*it)[j]);
          }
        }
      }
    }

    // // debug, output graph.
    // for (auto it1 = e.begin(); it1 != e.end(); ++it1) {
    //   cout << it1->first << ": ";
    //   for (auto it2 = (it1->second).begin(); it2 != (it1->second).end(); ++it2) {
    //     cout << *it2 << " ";
    //   }
    //   cout << endl;
    // }

    string source = "Erdos, P.";
    v[source]._score = 0;
    v[source]._visited = true;
    // run breadth-first search
    bfs(source, e, v);

    cout << "Scenario " << scenario_id << endl;
    for (size_t i = 0; i < n; ++i) {
      getline(cin, name);
      if (v[name]._score != INF) {
        cout << name << " " << v[name]._score << endl;
      } else {
        cout << name << " infinity\n";
      }
    }

    scenario_id++;
  }

  return 0;
}