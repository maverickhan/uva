/*
  UVA online judge Problem: Stacks of Flapjacks
  Key Idea: use priority queue to track current max and max id
  Flip id in a dictionary to keep the id-to-val mapping up-to-date.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

inline int reverse_id(size_t size, size_t idx, size_t ref_idx)
{
  return size - idx - 1 + ref_idx;
}

int main()
{
  string line;
  while (getline(cin, line)) {
    cout << line << endl;
    string num;
    stringstream ss(line);
    priority_queue<int> pq;
    vector<int> v;
    unordered_map<int, int> val2idx;

    int size = 0;
    while (getline(ss, num, ' ')) {
      int val = stoi(num);
      pq.push(val);
      v.push_back(val);
      size++;
    }

    for (int i = size-1; i >=0; --i) {
      val2idx.insert(pair<int, int>(v[size-i-1], i));
    }

    // start of the algorithm
    for (int i = 0; i < size; ++i) {
      int curr_max = pq.top();
      int curr_max_id = val2idx[curr_max];
      pq.pop();

      if (curr_max_id == i) {
        continue;
      } else {
        if (curr_max_id != size-1) {
          cout << curr_max_id+1 << " ";
          for (auto it = val2idx.begin(); it != val2idx.end(); ++it) {
            // need to reverse index for any element above current max
            // (including current max)
            if (it->second >= curr_max_id) {
              it->second = reverse_id(size, it->second, curr_max_id);
            }
          }
        }

        cout << i+1 << " ";
        for (auto it = val2idx.begin(); it != val2idx.end(); ++it) {
          // need to reverse index for any element above current index
          // (including current index) to let current max fall into place.
          if (it->second >= i) {
            it->second = reverse_id(size, it->second, i);
          }
        }
      }
    }
    cout << "0" << endl;

  }

  return 0;
}
