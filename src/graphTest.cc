#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "header/graph.hh"

using namespace std;

int main()
{
	// read in graph
	string line;
	graph g;
	while(getline(cin, line)) {
		stringstream ss(line);
		vector<string> vinfo;
		string str;
		while (getline(ss, str, ' ')) {
			vinfo.push_back(str);
		}

		g.add_vertex(vinfo[0]);
		for (size_t i = 1; i < vinfo.size(); i += 2) {
			g.add_edge(vinfo[0], vinfo[i], stoi(vinfo[i+1]));
		}
	}

	bfs("A", g, [](std::shared_ptr<vertex> p){
		cout << p->id << " ";
	});
	cout << endl;

	return 0;
}

