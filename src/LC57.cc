/*
  Leetcode problem: insert intervals
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

 struct Interval {
   int start;
   int end;
   Interval() : start(0), end(0) {}
   Interval(int s, int e) : start(s), end(e) {}
 };

int main()
{
	size_t n;
	cin >> n;
	vector<Interval> v1;
	int b, e;
	for (size_t i = 0; i < n; ++i) {
		cin >> b >> e;
		v1.emplace_back(Interval(b, e));
	}
	cin >> b >> e;
	Interval iv(b, e);

	if (n == 0) {
		cout << iv.start << " " << iv.end << endl;
	} else {
		vector<Interval> r1;
		vector<Interval> r2;
		int new_block_start = -1, new_block_end = -1;

		// irrelavent intervals at the beginning
		for (const auto& i : v1) {
			if (i.end < iv.start) {
				r1.push_back(i);
			} else if (i.start > iv.end) {
				r2.push_back(i);
			} else {
				if (new_block_start == -1) {
					new_block_start = min(i.start, iv.start);
				}
				new_block_end = max(i.end, iv.end);
			}
		}

		r1.emplace_back(Interval(new_block_start, new_block_end));
		r1.insert(r1.end(), r2.begin(), r2.end());

		for (const auto& i : r1) {
			cout << i.start << " " << i.end << endl;
		}
	}

  return 0;
}

