/*
  UVA online judge Problem P846: Steps
  First attempt is to use BFS, which takes a lot of time
  For each state, you have curr_val, step_size, and length

  Second attempt is to discover F(n) where n is the number of steps, and F(n)
  calculates the maximum number n steps could reach.
  When we use binary search to figure out maximum steps within [0, y-x]
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

size_t fn(size_t nsteps)
{
  size_t max_num;

  if (nsteps % 2 == 0) {
    max_num = (nsteps * nsteps + 2 * nsteps) / 4;
  } else {
    max_num = (nsteps * nsteps + 2 * nsteps + 1) / 4;
  }

  return max_num;
}

size_t find_min_steps(size_t x, size_t y)
{
  // base case
  if (x == y)
    return 0;

  size_t lb = 0;
  size_t ub = y - x;
  size_t target = ub;

  // binary search to find the maximum possible increment
  // we need to find the first maxium increment >= target
  // this is why we cannot let ub = mid-1 in the else clause.
  while (lb < ub) {
    size_t mid = (lb + ub) / 2;
    size_t val = fn(mid);
    if (val == target) {
      return mid;
    } else if (val < target) {
      lb = mid + 1;
    } else {
      ub = mid;
    }
  }

  return ub;
}

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();

  size_t x, y;
  for (size_t i = 0; i < ntest; ++i) {
    cin >> x >> y;
    cin.ignore();

    size_t steps = find_min_steps(x, y);
    cout << steps << endl;
  }

  return 0;
}
