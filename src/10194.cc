/*
  UVA online judge Problem P10194: Football (aka soccer)
  Note: unorderd_map cannot be ordered using std::order
  Use std::transform to transform original string to all lower cases to enable
  case-insensitive comparison.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

struct record{
  int points;
  int games;
  int win;
  int tie;
  int lose;
  int goal_diff;
  int goal_score;
  int goal_against;

  record(int pt = 0,
       int g = 0,
       int w = 0,
       int t = 0,
       int l = 0,
       int gd = 0,
       int gs = 0,
       int ga = 0
      ) : points(pt),games(g),win(w),tie(t),lose(l),
          goal_diff(gd),goal_score(gs),goal_against(ga)
  {}

  record(const record& rhs) 
  {
    if (this != &rhs) {
      points = rhs.points;
      games = rhs.games;
      win = rhs.win;
      tie = rhs.tie;
      lose = rhs.lose;
      goal_diff = rhs.goal_diff;
      goal_score = rhs.goal_score;
      goal_against = rhs.goal_against;
    }
  }
};

struct team : public record {
  record r;
  string name;
  team(const record& r, const string& s) : 
       r(r), name(s) {}
};

int main()
{
  size_t ntest;
  cin >> ntest;
  cin.ignore();
  string line;
  for (size_t i = 0; i < ntest; ++i) {
    getline(cin, line);
    cout << line << endl;
    size_t nteam;
    cin >> nteam;
    cin.ignore();
    unordered_map<string, record> standing;
    for (size_t i = 0; i < nteam; ++i) {
      getline(cin, line);
      standing.insert(pair<string, record>(line, record()));
    }

    size_t ngame;
    cin >> ngame;
    cin.ignore();

    // process games
    for (size_t i = 0; i < ngame; ++i) {
      getline(cin, line);
      stringstream ss1(line);
      string home, away, score;
      getline(ss1, home, '#');
      getline(ss1, score, '#');
      getline(ss1, away, '#');

      string home_goal, away_goal;
      stringstream ss2(score);
      getline(ss2, home_goal, '@');
      getline(ss2, away_goal, '@');

      int hg = stoi(home_goal);
      int ag = stoi(away_goal);

      standing[home].games += 1;
      standing[away].games += 1;

      if (hg > ag) {
        int diff = hg - ag;
        standing[home].points += 3;
        standing[home].win += 1;
        standing[home].goal_diff += diff;
        standing[home].goal_score += hg;
        standing[home].goal_against += ag;
        standing[away].lose += 1;
        standing[away].goal_diff -= diff;
        standing[away].goal_score += ag;
        standing[away].goal_against += hg;
      } else if (hg == ag) {
        standing[home].points += 1;
        standing[home].tie += 1;
        standing[home].goal_score += hg;
        standing[home].goal_against += ag;
        standing[away].points += 1;
        standing[away].tie += 1;
        standing[away].goal_score += ag;
        standing[away].goal_against += hg;
      } else {
        int diff = ag - hg;
        standing[away].points += 3;
        standing[away].win += 1;
        standing[away].goal_diff += diff;
        standing[away].goal_score += ag;
        standing[away].goal_against += hg;
        standing[home].lose += 1;
        standing[home].goal_diff -= diff;
        standing[home].goal_score += hg;
        standing[home].goal_against += ag;
      }
    }

    vector<team> v;
    for (const auto& i : standing) {
      v.push_back(team(i.second, i.first));
    }

    sort(v.begin(), v.end(), [](const team& a, const team& b){
      if (a.r.points > b.r.points) return true;
      else if (a.r.points == b.r.points) {
        if (a.r.win > b.r.win) return true;
        else if (a.r.win == b.r.win) {
          if (a.r.goal_diff > b.r.goal_diff) return true;
          else if (a.r.goal_diff == b.r.goal_diff) {
            if (a.r.goal_score > b.r.goal_score) return true;
            else if (a.r.goal_score == b.r.goal_score) {
              if (a.r.games < b.r.games) return true;
              else if (a.r.games == b.r.games) {
                // transform team names to case insensitive
                string htn(a.name);
                string atn(b.name);
                transform(htn.begin(), htn.end(), htn.begin(), ::tolower);
                transform(atn.begin(), atn.end(), atn.begin(), ::tolower);            
                if (htn.compare(atn) < 0) return true;
              }
            }
          }
        }
      }

      return false;
    });

    for (size_t i = 0; i < v.size(); ++i) {
      cout << i+1 << ") ";
      cout << v[i].name << " ";
      cout << v[i].r.points << "p, ";
      cout << v[i].r.games << "g ";
      cout << "(" << v[i].r.win << "-" << v[i].r.tie << "-" << v[i].r.lose << "), ";
      cout << v[i].r.goal_diff << "gd ";
      cout << "(" << v[i].r.goal_score << "-" << v[i].r.goal_against << ")" << endl;
    }

    if (i != ntest-1) {
      cout << endl;
    }
  }

  return 0;
}
