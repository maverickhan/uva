/*
  UVA online judge Problem #199: partial differential equation
  https://uva.onlinejudge.org/external/1/199.pdf
 */

#include "header/common.hh"

using namespace std;
using namespace uva;

int main()
{
  string test_id;
  while(getline(cin, test_id)) {

    // Read in input batch $test_id
    int n;
    read_number(n);

    vector<vector<int>> v(3);
    vector<vector<int>> g(3);
    vector<vector<int>> b(4);
    vector<vector<int>> f(n+1);
    for (int i = 2; i >= 0; --i) {
      read_vector(v[i]);
    }
    for (int i = 2; i >= 0; --i) {
      read_vector(g[i]);
    }
    for (int i = 0; i < 4; ++i) {
      read_vector(b[i]);
    }
    for (int i = n; i >= 0; --i) {
      read_vector(f[i]);
    }

    // result matrix
    size_t dim = (n-1) * (n-1);
    vector<vector<int>> A(dim, vector<int>(dim, 0));
    vector<int> B(dim, 0);
    int c = n * n;

    // Iterate Point (i,j) in inner point space.
    // Overall space is (n + 1) X (n + 1)
    // Inner points dimension (n - 2) X (n - 2)

    int cnt = 0;
    for (int i = n-1; i > 0; --i) {
      for (int j = 1; j < n; ++j) {

        int sum = 0;
        // Iterate over 3 X 3 sub-matrix v & g
        for (int p = i+1, x = 2; p >= i-1; --p, --x) {
          for (int q = j-1, y = 0; q <= j+1; ++q, ++y) {
            // Matrix multiplication
            sum += g[x][y] * f[p][q];
            if (p == 0) { // boundary
              sum -= c * v[x][y] * b[0][q];
              continue;
            } else if (p == n) { // boundary
              sum -= c * v[x][y] * b[1][q];
              continue;
            } else if (q == 0) { // boundary
              sum -= c * v[x][y] * b[2][p];
              continue;
            } else if (q == n) { //boundary
              sum -= c * v[x][y] * b[3][p];
              continue;
            } else {
                int row = cnt;
                int col = (n-1) * (n-1-p) + (q-1);
                A[row][col] = c * v[x][y];
            }
          }
        }

        B[cnt] = sum;
        cnt++;
      }
    }

    for (int i = 0; i < dim; ++i) {
      print_vector(A[i]);
    }
    print_vector(B);

  }

	return 0;
}
