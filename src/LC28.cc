/*
  UVA online judge Problem :
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

bool match(const string& s1, size_t pos, size_t n, const string& s2,
           const char c, size_t& new_next, bool& found_new) {
  bool first_encounter = true;
  for (size_t i = pos; i < n; ++i) {
    if (s1[i] == c && first_encounter) {
      new_next = i;
      first_encounter = false;
      found_new = true;
    }
    if (s1[i] == s2[i-pos+1]) continue;
    else return false;
  }

  return true;
}

int strStr(const string& s1, const string& s2)
{
  if (s1.empty() || s2.empty())
    return -1;

  char c = s2[0];
  size_t sz1 = s1.size();
  size_t sz2 = s2.size();

  if (sz1 < sz2) {
    return -1;
  }

  size_t next_pos = sz1+1;
  bool found_new = false;

  for (size_t i = 0; i < sz1; ++i) {
    if (s1[i] == c) {
      if (match(s1, i+1, i+sz2-1, s2, c, next_pos, found_new)) {
        return i;
      } else {
        if (found_new) i = next_pos;
        else i = i + sz2 - 1;
      }
    }
  }

  return -1;
}

int main()
{
  int ntest;
  cin >> ntest;
  cin.ignore();

  for (int k = 0; k < ntest; ++k) {
    string s1, s2;
    getline(cin, s1);
    getline(cin, s2);

    int pos = strStr(s1, s2);
    cout << pos << endl;
  }

  return 0;
}

