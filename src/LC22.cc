/*
  Leetcode Problem 22: Generate Parenthesis
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

void genParenthesis(vector<char>& r, int lp, int rp)
{
  // base case
  if (lp == 0) {
    for (int i = 0; i < rp; ++i) {
      r.push_back(')');
    }
    // output result
    for (const auto& i : r) {
      cout << i << ' ';
    }
    cout << endl;

    for (int i = 0; i < rp; ++i) {
      r.pop_back();
    }

    return ;
  }

  // recursive generate and backtrack when necessary
  // option 1: append a left parenthesis
  r.push_back('(');
  genParenthesis(r, lp-1, rp);
  r.pop_back();

  // option 2: if rp > lp, append a right parenthesis and backtrack
  if (rp > lp) {
    r.push_back(')');
    genParenthesis(r, lp, rp-1);
    r.pop_back();
  }
}

int main()
{
  int n;

  while (cin >> n) {
    int lp = n;
    int rp = n;

    vector<char> result;
    result.push_back('(');

    genParenthesis(result, lp-1, rp);
    cout << endl;
  }

  return 0;
}

