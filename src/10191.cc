/*
  UVA online judge Problem P10091: Longest Nap
  Problem will become complicated if appointment intervals can
  overlap, like the case in input2.txt. Fortunatelly this is
  not the assumption for this problem.
 */

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

// start and end calculated as minutes passing 00:00
struct interval {
  int start;
  int end;

  interval() {
    start = 0;
    end = 0;
  }
  interval(int s, int e) : start(s), end(e) {}
  interval& operator=(const interval& rhs) {
    if (this != &rhs) {
      start = rhs.start;
      end = rhs.end;
    }
    return *this;
  }
};

int time2min(const string& s) 
{
  stringstream ss(s);
  string t;
  int min = 0;

  getline(ss, t, ':');
  min += stoi(t) * 60;
  getline(ss, t, ':');
  min += stoi(t);

  return min;
}

int main()
{
  size_t day = 1;
  size_t n;
  string line;
  while (cin >> n) {
    cin.ignore();
    vector<interval> va;
    va.push_back(interval(600, 600));
    va.push_back(interval(1080, 1080));
    for (size_t i = 0; i < n; ++i) {
      getline(cin, line);
      stringstream ss(line);
      string s, e;
      getline(ss, s, ' ');
      getline(ss, e, ' ');
      int start = time2min(s);
      int end = time2min(e);
      va.push_back(interval(start, end));
    }

    sort(va.begin(), va.end(), [](const interval& a, const interval& b){
      if (a.start < b.start) return true;
      else if (a.start == b.start) {
        if (a.end < b.end) {
          return true;
        }
      }
      return false;
    });

    vector<interval> vb(va.size()-1);
    for (size_t i = 1; i < va.size(); ++i) {
      vb[i-1] = interval(va[i-1].end, va[i].start);
    }

    int nap_length = vb[0].end - vb[0].start;
    int hour = vb[0].start / 60;
    int min = vb[0].start % 60;

    for (size_t i = 1; i < vb.size(); ++i) {
      int length = vb[i].end - vb[i].start;
      if ((length > nap_length) || (length == nap_length && vb[i].start < hour * 60)) {
        nap_length = length;
        hour = vb[i].start / 60;
        min = vb[i].start % 60;
      }
    }

    cout << "Day #" << day << ": ";
    cout << "the longest nap starts at " << hour << ":";
    if (min < 10) {
      cout << "0";
    }
    cout << min;
    cout << " and will last for ";
    if (nap_length >= 60) {
      cout << nap_length / 60 << " hours and ";
    }
    cout << nap_length % 60 << " minutes." << endl;

    day++;
  }

  return 0;
}
