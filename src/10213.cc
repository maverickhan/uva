/*
  UVA online judge Problem P10231:
  Maximum occurs when no three lines intersect.
  The original region is 1
  Each line adds one to the new region
  Each quadralateral adds one to the new region
*/

#include <algorithm>
#include <bitset>
#include <climits>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <locale>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "header/infint.hh"

using namespace std;

int main()
{
  size_t ntests;
  cin >> ntests;
  cin.ignore();
  for (size_t i = 0; i < ntests; ++i) {
    size_t n;
    cin >> n;
    cin.ignore();

    if (n == 0) cout << "1" << endl;
    else if (n == 1) cout << "1" << endl;
    else if (n == 2) cout << "2" << endl;
    else if (n == 3) cout << "4" << endl;
    else {
      bigint result("1");
      bigint t = n * (n-1);

      result += t / 2;
      result += t * (n-2) * (n-3) / 24;

      cout << result << endl;
    }
  }

  return 0;
}

